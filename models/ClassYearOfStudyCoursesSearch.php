<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClassYearOfStudyCourses;

/**
 * ClassYearOfStudyCoursesSearch represents the model behind the search form about `app\models\ClassYearOfStudyCourses`.
 */
class ClassYearOfStudyCoursesSearch extends ClassYearOfStudyCourses
{
    public function rules()
    {
        return [
            [['cyos_course_id', 'cy_id', 'course_offering_id', 'pass_grade_id', 'contribute_to_final_result', 'is_core', 'school_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $condition = "1=1")
    {
        $query = ClassYearOfStudyCourses::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'cyos_course_id' => $this->cyos_course_id,
            'cy_id' => $this->cy_id,
            'course_offering_id' => $this->course_offering_id,
            'pass_grade_id' => $this->pass_grade_id,
            'contribute_to_final_result' => $this->contribute_to_final_result,
            'is_core' => $this->is_core,
            'school_id' => $this->school_id,
        ]);

        return $dataProvider;
    }
}
