<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auth_assignment".
 *
 * @property integer $assignment_id
 * @property string $item_name
 * @property string $user_id
 * @property integer $institution_structure_id
 * @property integer $created_at
 */
class AuthAssignment extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'auth_assignment';
    }


    
    public static function primaryKey() {
        return ['assignment_id'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['item_name'], 'required'],
            [['institution_structure_id', 'created_at'], 'integer'],
            [['item_name'], 'string', 'max' => 255],
            [['user_id'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'assignment_id' => 'Assignment ID',
            'item_name' => 'Item Name',
            'user_id' => 'User ID',
            'institution_structure_id' => 'Institution Structure',
            'created_at' => 'Created At',
        ];
    }
    

    public function getInstitutionStructure() {
        return $this->hasOne(InstitutionStructure::className(), ['institution_structure_id' => 'institution_structure_id']);
    }
    
    public function getItemName() {
        return $this->hasOne(AuthItem::className(), ['name' => 'item_name']);
    }

}
