<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentStatus;

/**
 * StudentStatusSearch represents the model behind the search form about `app\models\StudentStatus`.
 */
class StudentStatusSearch extends StudentStatus
{
    public function rules()
    {
        return [
            [['student_status_id'], 'integer'],
            [['student_status'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StudentStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'student_status_id' => $this->student_status_id,
        ]);

        $query->andFilterWhere(['like', 'student_status', $this->student_status]);

        return $dataProvider;
    }
}
