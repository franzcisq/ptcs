<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_year".
 *
 * @property integer $fy_id
 * @property integer $academic_year_id
 * @property integer $school_id
 * @property integer $year_number
 * @property integer $grading_system_version_id
 * @property integer $study_year
 * @property integer $required_year_units
 * @property integer $form_year_status_id
 * @property integer $approval_status_id
 * @property integer $publish_status_id
 *
 * @property AcademicYears $academicYear
 * @property GradingSystemVersions $gradingSystemVersion
 */
class FormYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_year_id', 'school_id', 'year_number', 'grading_system_version_id', 'study_year', 'form_year_status_id', 'approval_status_id', 'publish_status_id'], 'required'],
            [['academic_year_id', 'school_id', 'year_number', 'grading_system_version_id', 'study_year', 'required_year_units', 'form_year_status_id', 'approval_status_id', 'publish_status_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fy_id' => 'Fy ID',
            'academic_year_id' => 'Academic Year ID',
            'school_id' => 'School ID',
            'year_number' => 'Year Number',
            'grading_system_version_id' => 'Grading System Version ID',
            'study_year' => 'Study Year',
            'required_year_units' => 'Required Year Units',
            'form_year_status_id' => 'Form Year Status ID',
            'approval_status_id' => 'Approval Status ID',
            'publish_status_id' => 'Publish Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(AcademicYears::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradingSystemVersion()
    {
        return $this->hasOne(GradingSystemVersions::className(), ['grading_system_version_id' => 'grading_system_version_id']);
    }
    
       static public function getFormYearDesc($study_year) {
        
        switch ($study_year) {
            case '1':
                return 'Form One';

                break;
            
              case '2':
                return 'Form Two';

                break;
            
              case '3':
                return 'Form Three';

                break;
            
              case '4':
                return 'Form Four';

                break;
             

        }
        
    }
}
