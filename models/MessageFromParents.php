<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_from_parents".
 *
 * @property integer $msg_frm_parent_id
 * @property integer $school_id
 * @property integer $sent_by
 * @property integer $receiver
 * @property string $message
 * @property string $date_sent
 * @property integer $status
 */
class MessageFromParents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_from_parents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'sent_by', 'receiver', 'message', 'status'], 'required'],
            [['school_id', 'sent_by', 'receiver', 'status'], 'integer'],
            [['message'], 'string'],
            [['date_sent'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'msg_frm_parent_id' => 'Msg Frm Parent ID',
            'school_id' => 'School ID',
            'sent_by' => 'Sent By',
            'receiver' => 'Receiver',
            'message' => 'Message',
            'date_sent' => 'Date Sent',
            'status' => 'Status',
        ];
    }
}
