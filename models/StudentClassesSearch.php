<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentClasses;

/**
 * StudentClassesSearch represents the model behind the search form about `app\models\StudentClasses`.
 */
class StudentClassesSearch extends StudentClasses
{
    public function rules()
    {
        return [
            [['sc_id', 'student_year_of_study_id', 'cyos_id', 'cyos_status_id'], 'integer'],
            [['marks_obtained'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StudentClasses::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sc_id' => $this->sc_id,
            'student_year_of_study_id' => $this->student_year_of_study_id,
            'cyos_id' => $this->cyos_id,
            'cyos_status_id' => $this->cyos_status_id,
            'marks_obtained' => $this->marks_obtained,
        ]);

        return $dataProvider;
    }
}
