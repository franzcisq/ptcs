<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentYearStatus;

/**
 * StudentYearStatusSearch represents the model behind the search form about `app\models\StudentYearStatus`.
 */
class StudentYearStatusSearch extends StudentYearStatus
{
    public function rules()
    {
        return [
            [['student_year_status_id'], 'integer'],
            [['status_desc'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StudentYearStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'student_year_status_id' => $this->student_year_status_id,
        ]);

        $query->andFilterWhere(['like', 'status_desc', $this->status_desc]);

        return $dataProvider;
    }
}
