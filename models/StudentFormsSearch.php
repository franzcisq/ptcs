<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentForms;

/**
 * StudentFormsSearch represents the model behind the search form about `app\models\StudentForms`.
 */
class StudentFormsSearch extends StudentForms
{
    public function rules()
    {
        return [
            [['sf_id', 'student_year_of_study_id', 'fyos_id', 'fyos_status_id'], 'integer'],
            [['marks_obtained'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StudentForms::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sf_id' => $this->sf_id,
            'student_year_of_study_id' => $this->student_year_of_study_id,
            'fyos_id' => $this->fyos_id,
            'fyos_status_id' => $this->fyos_status_id,
            'marks_obtained' => $this->marks_obtained,
        ]);

        return $dataProvider;
    }
}
