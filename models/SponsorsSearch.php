<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sponsors;

/**
 * SponsorsSearch represents the model behind the search form about `app\models\Sponsors`.
 */
class SponsorsSearch extends Sponsors
{
    public function rules()
    {
        return [
            [['sponsor_id', 'is_active'], 'integer'],
            [['sponsor_name', 'sponsor_description', 'address', 'e_mail', 'phone_no', 'fax', 'logo'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Sponsors::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sponsor_id' => $this->sponsor_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'sponsor_name', $this->sponsor_name])
            ->andFilterWhere(['like', 'sponsor_description', $this->sponsor_description])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'e_mail', $this->e_mail])
            ->andFilterWhere(['like', 'phone_no', $this->phone_no])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        return $dataProvider;
    }
}
