<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schools".
 *
 * @property integer $school_id
 * @property integer $school_type_id
 * @property integer $region_id
 * @property integer $district_id
 * @property integer $ward_id
 * @property integer $street_id
 * @property string $school_name
 * @property string $school_description
 * @property string $address
 * @property string $phone_no
 * @property string $e_mail
 * @property string $fax
 * @property string $logo
 * @property integer $start_year_id
 * @property string $google_location
 * @property integer $school_privacy_statement_id
 * @property integer $school_board_id
 * @property integer $school_donor_id
 * @property integer $school_sponsor_id
 * @property integer $school_gallery_id
 * @property integer $school_motto_id
 * @property integer $headmaster_statement_id
 * @property integer $msg_frm_teacher_id
 * @property integer $msg_frm_parent_id
 * @property integer $inbox_id
 */
class Schools extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schools';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_type_id', 'region_id', 'district_id', 'ward_id', 'street_id', 'school_name', 'address', 'phone_no', 'e_mail', 'fax', 'start_year_id', 'school_privacy_statement_id', 'school_board_id', 'school_donor_id', 'school_sponsor_id', 'school_gallery_id', 'school_motto_id', 'headmaster_statement_id', 'msg_frm_teacher_id', 'msg_frm_parent_id', 'inbox_id','is_active'], 'required'],
            [['school_type_id', 'region_id', 'district_id', 'ward_id', 'street_id', 'start_year_id', 'school_privacy_statement_id', 'school_board_id', 'school_donor_id', 'school_sponsor_id', 'school_gallery_id', 'headmaster_statement_id', 'msg_frm_teacher_id', 'msg_frm_parent_id', 'inbox_id','is_active'], 'integer'],
            [['school_description', 'google_location'], 'string'],
            [['school_name','school_motto'], 'string', 'max' => 300],
            [['address', 'phone_no', 'e_mail', 'fax', 'logo'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_id' => 'School',
            'school_type_id' => 'School Type',
            'region_id' => 'Region',
            'district_id' => 'District',
            'ward_id' => 'Ward',
            'street_id' => 'Street',
            'school_name' => 'School Name',
            'school_description' => 'Description',
            'address' => 'Address',
            'phone_no' => 'Phone#',
            'e_mail' => 'E-Mail',
            'fax' => 'Fax',
            'logo' => 'Logo',
            'start_year_id' => 'Start Year',
            'google_location' => 'Location Map',
            'school_privacy_statement_id' => 'School Privacy Statement',
            'school_board_id' => 'School Board',
            'school_donor_id' => 'School Donor',
            'school_sponsor_id' => 'School Sponsor',
            'school_gallery_id' => 'School Gallery',
            'school_motto' => 'School Motto',
            'headmaster_statement_id' => 'Headmaster Statement',
            'msg_frm_teacher_id' => 'Msg Frm Teacher',
            'msg_frm_parent_id' => 'Msg Frm Parent',
            'inbox_id' => 'Inbox',
            'is_active'=> 'IsActive',
        ];
    }
}
