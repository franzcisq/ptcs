<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CourseOffering;

/**
 * CourseOfferingSearch represents the model behind the search form about `app\models\CourseOffering`.
 */
class CourseOfferingSearch extends CourseOffering
{
    public function rules()
    {
        return [
            [['course_offering_id', 'admistered_by', 'course_id', 'course_offering_version_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition)
    {
        $query = CourseOffering::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'course_offering_id' => $this->course_offering_id,
            'admistered_by' => $this->admistered_by,
            'course_id' => $this->course_id,
            'course_offering_version_id' => $this->course_offering_version_id,
        ]);

        return $dataProvider;
    }
}
