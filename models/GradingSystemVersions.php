<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grading_system_versions".
 *
 * @property integer $grading_system_version_id
 * @property string $description
 *
 * @property ClassYear[] $classYears
 * @property FormYear[] $formYears
 * @property GradingSystem[] $gradingSystems
 */
class GradingSystemVersions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grading_system_versions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_type_id'], 'integer'],
            [['description','school_type_id'], 'required'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'grading_system_version_id' => 'Grading Score System',
            'description' => 'Description',
             'school_type_id'=>'School Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassYears()
    {
        return $this->hasMany(ClassYear::className(), ['grading_system_version_id' => 'grading_system_version_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormYears()
    {
        return $this->hasMany(FormYear::className(), ['grading_system_version_id' => 'grading_system_version_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradingSystems()
    {
        return $this->hasMany(GradingSystem::className(), ['grading_system_version_id' => 'grading_system_version_id']);
    }
}
