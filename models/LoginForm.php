<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model {

    public $username;
    public $password;
    public $rememberMe = true;
    private $_user = false;
    public $captcha;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
//            ['captcha', 'captcha'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login() {
        if ($this->validate()) {
            $login_attempts = new \app\models\LoginAttempts();
            $login_attempts->user_id = Users::findByUsername($this->username)->user_id;
            $login_attempts->ip_address = Yii::$app->getRequest()->getUserIP();
            $login_attempts->successfully_attempt = 1;
            $login_attempts->save(false);
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            
            $user_id = Users::findByUsername($this->username)->user_id;
            
            if ($user_id !=NULL) {
            $ip_address = Yii::$app->getRequest()->getUserIP();
            $sql = 'SELECT * FROM `login_attempts` WHERE user_id = '.$user_id.' AND `successfully_attempt` = 0 AND `ip_address` = "'.$ip_address.'" AND `last_login` = TIMESTAMPDIFF(SECOND,(NOW() + INTERVAL 30 SECOND),NOW()) <= 30';
            $unsuccessfully_attempt = count(LoginAttempts::findBySql($sql)->all());
            if ($unsuccessfully_attempt >=3) {
                // block user from login attempts
                $this->addError('username', 'Your blocked! Try again after 30 Minutes');
                return false; 
            }  else {
            $login_attempts = new \app\models\LoginAttempts();
            $login_attempts->user_id = $user_id;
            $login_attempts->ip_address = $ip_address;
            $login_attempts->successfully_attempt = 0;
            $login_attempts->save(false);
            return false;    
            }
            
             }  else {
                  $this->addError('username', 'Sorry! Your not user of this system');
                 return false; 
             }
          
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        if ($this->_user === false) {
            $this->_user = Users::findByUsername($this->username);
        }

        return $this->_user;
    }

}
