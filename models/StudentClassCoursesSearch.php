<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentClassCourses;

/**
 * StudentClassCoursesSearch represents the model behind the search form about `app\models\StudentClassCourses`.
 */
class StudentClassCoursesSearch extends StudentClassCourses
{
    public function rules()
    {
        return [
            [['scc_id', 'cy_id', 'cyos_course_id', 'score_type', 'grade_scored', 'student_class_course_status_id', 'last_updated'], 'integer'],
            [['result', 'remarks', 'changes_log'], 'safe'],
            [['grade_points'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = StudentClassCourses::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'scc_id' => $this->scc_id,
            'cy_id' => $this->cy_id,
            'cyos_course_id' => $this->cyos_course_id,
            'score_type' => $this->score_type,
            'grade_scored' => $this->grade_scored,
            'student_class_course_status_id' => $this->student_class_course_status_id,
            'grade_points' => $this->grade_points,
            'last_updated' => $this->last_updated,
        ]);

        $query->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'changes_log', $this->changes_log]);

        return $dataProvider;
    }
}
