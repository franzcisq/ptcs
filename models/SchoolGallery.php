<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school_gallery".
 *
 * @property integer $school_gallery_id
 * @property integer $school_id
 * @property string $gallery
 * @property integer $is_visible
 * @property string $date_uploaded
 */
class SchoolGallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'gallery', 'is_visible'], 'required'],
            [['school_id', 'is_visible'], 'integer'],
            [['date_uploaded'], 'safe'],
            [['gallery'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_gallery_id' => 'School Gallery ID',
            'school_id' => 'School ID',
            'gallery' => 'Gallery',
            'is_visible' => 'Is Visible',
            'date_uploaded' => 'Date Uploaded',
        ];
    }
}
