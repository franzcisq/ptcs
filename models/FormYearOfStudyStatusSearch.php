<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormYearOfStudyStatus;

/**
 * FormYearOfStudyStatusSearch represents the model behind the search form about `app\models\FormYearOfStudyStatus`.
 */
class FormYearOfStudyStatusSearch extends FormYearOfStudyStatus
{
    public function rules()
    {
        return [
            [['form_year_of_study_status_id'], 'integer'],
            [['status'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = FormYearOfStudyStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'form_year_of_study_status_id' => $this->form_year_of_study_status_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
