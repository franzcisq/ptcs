<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BoardMembersStatement;

/**
 * BoardMembersStatementSearch represents the model behind the search form about `app\models\BoardMembersStatement`.
 */
class BoardMembersStatementSearch extends BoardMembersStatement
{
    public function rules()
    {
        return [
            [['board_members_statement_id', 'school_board_id', 'is_active'], 'integer'],
            [['statement', 'date_created'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = BoardMembersStatement::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'board_members_statement_id' => $this->board_members_statement_id,
            'school_board_id' => $this->school_board_id,
            'date_created' => $this->date_created,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'statement', $this->statement]);

        return $dataProvider;
    }
}
