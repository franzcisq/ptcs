<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VwPrimaryStudents;

/**
 * VwPrimaryStudentsSearch represents the model behind the search form about `app\models\VwPrimaryStudents`.
 */
class VwPrimaryStudentsSearch extends VwPrimaryStudents
{
    public function rules()
    {
        return [
            [['user_id', 'student_status_id', 'school_id', 'is_active', 'login_counts', 'user_type', 'staff_position_id', 'primary_student_year_of_study_id', 'cy_id', 'student_year_status_id', 'year_of_study', 'year_units', 'status'], 'integer'],
            [['profile_picture', 'registration_number', 'surname', 'firstname', 'middlename', 'sex', 'date_of_birth', 'place_of_birth', 'mailing_address', 'telephone_no', 'email_address', 'admission_date', 'status_comments', 'completion_date', 'username', 'password', 'last_login', 'salutation', 'fullname'], 'safe'],
            [['results', 'year_grade_points'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = VwPrimaryStudents::find();
        $query->orderBy("year_of_study ASC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'student_status_id' => $this->student_status_id,
            'school_id' => $this->school_id,
            'admission_date' => $this->admission_date,
            'completion_date' => $this->completion_date,
            'is_active' => $this->is_active,
            'login_counts' => $this->login_counts,
            'last_login' => $this->last_login,
            'user_type' => $this->user_type,
            'staff_position_id' => $this->staff_position_id,
            'primary_student_year_of_study_id' => $this->primary_student_year_of_study_id,
            'cy_id' => $this->cy_id,
            'student_year_status_id' => $this->student_year_status_id,
            'year_of_study' => $this->year_of_study,
            'results' => $this->results,
            'year_units' => $this->year_units,
            'year_grade_points' => $this->year_grade_points,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'profile_picture', $this->profile_picture])
            ->andFilterWhere(['like', 'registration_number', $this->registration_number])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'middlename', $this->middlename])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'date_of_birth', $this->date_of_birth])
            ->andFilterWhere(['like', 'place_of_birth', $this->place_of_birth])
            ->andFilterWhere(['like', 'mailing_address', $this->mailing_address])
            ->andFilterWhere(['like', 'telephone_no', $this->telephone_no])
            ->andFilterWhere(['like', 'email_address', $this->email_address])
            ->andFilterWhere(['like', 'status_comments', $this->status_comments])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'salutation', $this->salutation])
            ->andFilterWhere(['like', 'fullname', $this->fullname]);

        return $dataProvider;
    }
}
