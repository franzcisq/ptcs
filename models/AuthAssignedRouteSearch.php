<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AuthAssignedRoute;

/**
 * AuthAssignedRouteSearch represents the model behind the search form about `app\models\AuthAssignedRoute`.
 */
class AuthAssignedRouteSearch extends AuthAssignedRoute
{
    public function rules()
    {
        return [
            [['auth_assigned_route_id', 'user_id', 'institution_structure_id'], 'integer'],
            [['auth_item_name', 'description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $condition = "1=1")
    {
        $query = AuthAssignedRoute::find();
        $query->andWhere($condition);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'auth_assigned_route_id' => $this->auth_assigned_route_id,
            'user_id' => $this->user_id,
            'institution_structure_id' => $this->institution_structure_id,
        ]);

        $query->andFilterWhere(['like', 'auth_item_name', $this->auth_item_name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
