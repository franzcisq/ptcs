<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "academic_years".
 *
 * @property integer $academic_year_id
 * @property string $academic_year
 * @property integer $order
 * @property integer $is_current
 *
 * @property ClassYear[] $classYears
 * @property FormYear[] $formYears
 */
class AcademicYears extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'academic_years';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['academic_year', 'order', 'is_current'], 'required'],
            [['order', 'is_current'], 'integer'],
            [['academic_year'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'academic_year_id' => 'Academic Year ID',
            'academic_year' => 'Academic Year',
            'order' => 'Order',
            'is_current' => 'Is Current',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassYears() {
        return $this->hasMany(ClassYear::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormYears() {
        return $this->hasMany(FormYear::className(), ['academic_year_id' => 'academic_year_id']);
    }

    public static function getAcademicYears($school_id) {
        $data = self::findBySql(" SELECT academic_year_id AS id, academic_year as name  FROM  academic_years  WHERE  academic_year_id IN  ( SELECT academic_year_id FROM class_year WHERE school_id = {$school_id}) ")->asArray()->all();
        $value = (count($data) == 0) ? ['' => ''] : $data;
        return $value;
    }
    
    
       public static function getFormAcademicYears($school_id) {
        $data = self::findBySql(" SELECT academic_year_id AS id, academic_year as name  FROM  academic_years  WHERE  academic_year_id IN  ( SELECT academic_year_id FROM form_year WHERE school_id = {$school_id}) ")->asArray()->all();
        $value = (count($data) == 0) ? ['' => ''] : $data;
        return $value;
    }

}
