<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school_board".
 *
 * @property integer $school_board_id
 * @property integer $school_id
 * @property string $name
 * @property string $description
 * @property integer $is_active
 */
class SchoolBoard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_board';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'name', 'description', 'is_active'], 'required'],
            [['school_id', 'is_active'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_board_id' => 'School Board ID',
            'school_id' => 'School ID',
            'name' => 'Name',
            'description' => 'Description',
            'is_active' => 'Is Active',
        ];
    }
}
