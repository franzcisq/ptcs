<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "primary_student_year_of_study".
 *
 * @property string $primary_student_year_of_study_id
 * @property string $user_id
 * @property integer $cy_id
 * @property integer $student_year_status_id
 * @property integer $year_of_study
 * @property double $results
 * @property integer $year_units
 * @property double $year_grade_points
 * @property integer $status
 */
class PrimaryStudentYearOfStudy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
     public $academic_year;
    
    
    public static function tableName()
    {
        return 'primary_student_year_of_study';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'cy_id', 'student_year_status_id', 'year_of_study', 'results', 'year_units', 'year_grade_points', 'status','academic_year'], 'required'],
            [['user_id', 'cy_id', 'student_year_status_id', 'year_of_study', 'year_units', 'status'], 'integer'],
            [['results', 'year_grade_points'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'primary_student_year_of_study_id' => 'Primary Student Year Of Study ID',
            'user_id' => 'User ID',
            'cy_id' => 'Cy ID',
            'student_year_status_id' => 'Student Year Status ID',
            'year_of_study' => 'Year Of Study',
            'results' => 'Results',
            'year_units' => 'Year Units',
            'year_grade_points' => 'Year Grade Points',
            'status' => 'Status',
        ];
    }
}
