<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auth_assigned_route".
 *
 * @property integer $auth_assigned_route_id
 * @property string $auth_item_name
 * @property integer $user_id
 * @property integer $institution_structure_id
 */
class AuthAssignedRoute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_assigned_route';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_item_name', 'user_id'], 'required'],
            [['user_id', 'institution_structure_id'], 'integer'],
            [['auth_item_name','description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'auth_assigned_route_id' => 'Auth Assigned Route ID',
            'auth_item_name' => 'Auth Item Name',
            'user_id' => 'User ID',
            'institution_structure_id' => 'Institution Level',
        ];
    }
}
