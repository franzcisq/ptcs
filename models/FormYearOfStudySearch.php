<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormYearOfStudy;

/**
 * FormYearOfStudySearch represents the model behind the search form about `app\models\FormYearOfStudy`.
 */
class FormYearOfStudySearch extends FormYearOfStudy
{
    public function rules()
    {
        return [
            [['fyos_id', 'fy_id', 'fyos_number', 'fyos_status_id', 'approval_status_id', 'publish_status_id'], 'integer'],
            [['fyos_description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = FormYearOfStudy::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fyos_id' => $this->fyos_id,
            'fy_id' => $this->fy_id,
            'fyos_number' => $this->fyos_number,
            'fyos_status_id' => $this->fyos_status_id,
            'approval_status_id' => $this->approval_status_id,
            'publish_status_id' => $this->publish_status_id,
        ]);

        $query->andFilterWhere(['like', 'fyos_description', $this->fyos_description]);

        return $dataProvider;
    }
}
