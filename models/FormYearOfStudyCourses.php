<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_year_of_study_courses".
 *
 * @property integer $fyos_course_id
 * @property integer $fy_id
 * @property integer $school_id
 * @property integer $course_offering_id
 * @property integer $pass_grade_id
 * @property integer $contribute_to_final_result
 * @property integer $is_core
 */
class FormYearOfStudyCourses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_year_of_study_courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fy_id', 'school_id', 'course_offering_id', 'pass_grade_id', 'contribute_to_final_result', 'is_core'], 'required'],
            [['fy_id', 'school_id', 'course_offering_id', 'pass_grade_id', 'contribute_to_final_result', 'is_core'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fyos_course_id' => 'Fyos Course ID',
            'fy_id' => 'Fy ID',
            'school_id' => 'School ID',
            'course_offering_id' => 'Course Offering ID',
            'pass_grade_id' => 'Pass Grade ID',
            'contribute_to_final_result' => 'Contribute To Final Result',
            'is_core' => 'Is Core',
        ];
    }
}
