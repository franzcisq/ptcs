<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inbox;

/**
 * InboxSearch represents the model behind the search form about `app\models\Inbox`.
 */
class InboxSearch extends Inbox
{
    public function rules()
    {
        return [
            [['inbox_id', 'message_id', 'sent_by', 'received_by', 'school_id', 'status'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Inbox::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'inbox_id' => $this->inbox_id,
            'message_id' => $this->message_id,
            'sent_by' => $this->sent_by,
            'received_by' => $this->received_by,
            'school_id' => $this->school_id,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
