<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "staff_positions".
 *
 * @property integer $staff_position_id
 * @property string $position_name
 */
class StaffPositions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff_positions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_name'], 'required'],
            [['position_name'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'staff_position_id' => 'Staff Position ID',
            'position_name' => 'Position Name',
        ];
    }
}
