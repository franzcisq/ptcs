<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentCourseStatus;

/**
 * StudentCourseStatusSearch represents the model behind the search form about `app\models\StudentCourseStatus`.
 */
class StudentCourseStatusSearch extends StudentCourseStatus
{
    public function rules()
    {
        return [
            [['student_course_status_id'], 'integer'],
            [['course_status', 'abbreviation'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StudentCourseStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'student_course_status_id' => $this->student_course_status_id,
        ]);

        $query->andFilterWhere(['like', 'course_status', $this->course_status])
            ->andFilterWhere(['like', 'abbreviation', $this->abbreviation]);

        return $dataProvider;
    }
}
