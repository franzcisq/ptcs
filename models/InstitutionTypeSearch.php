<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InstitutionType;

/**
 * InstitutionTypeSearch represents the model behind the search form about `app\models\InstitutionType`.
 */
class InstitutionTypeSearch extends InstitutionType
{
    public function rules()
    {
        return [
            [['institution_type_id', 'order'], 'integer'],
            [['institution_type_name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = InstitutionType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'institution_type_id' => $this->institution_type_id,
            'order' => $this->order,
        ]);

        $query->andFilterWhere(['like', 'institution_type_name', $this->institution_type_name]);

        return $dataProvider;
    }
}
