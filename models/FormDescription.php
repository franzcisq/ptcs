<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_description".
 *
 * @property integer $form_description_id
 * @property integer $form_number
 * @property string $description
 */
class FormDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_number', 'description'], 'required'],
            [['form_number'], 'integer'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'form_description_id' => 'Form Description ID',
            'form_number' => 'Form Number',
            'description' => 'Description',
        ];
    }
    
  
        public static function getFormYears($academic_year) {
        $data = self::findBySql(" SELECT form_number AS id, description as name  FROM  form_description  WHERE  form_number IN  ( SELECT study_year FROM form_year WHERE academic_year_id = {$academic_year}) ")->asArray()->all();
        $value = (count($data) == 0) ? ['' => ''] : $data;
        return $value;
    }
}
