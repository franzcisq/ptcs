<?php

namespace app\models;

use Yii;
use yii\base\Model;
use Swift_Validate;

/**
 * LoginForm is the model behind the login form.
 */
class ResetPassword extends Model {

    public $E_mail;
    public $Phone_number;
    public $captcha;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // username and password are both required
            [['E_mail', 'Phone_number','captcha'], 'required'],
            // rememberMe must be a boolean value
//            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            [['Phone_number'], 'string', 'max' => 200],
            [['E_mail'], 'email'],
             ['captcha','captcha'],
            
        ];
    }

    public function attributeLabels() {
        return [
            'E_mail' => 'E-mail Address',
            'Phone_number' => 'Mobile Phone Number',
             'captcha' => 'Verification Code',
        ];
    }

  

}


