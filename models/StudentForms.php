<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_forms".
 *
 * @property integer $sf_id
 * @property integer $student_year_of_study_id
 * @property integer $fyos_id
 * @property integer $fyos_status_id
 * @property double $marks_obtained
 */
class StudentForms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_forms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_year_of_study_id', 'fyos_id', 'fyos_status_id', 'marks_obtained'], 'required'],
            [['student_year_of_study_id', 'fyos_id', 'fyos_status_id'], 'integer'],
            [['marks_obtained'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sf_id' => 'Sf ID',
            'student_year_of_study_id' => 'Student Year Of Study ID',
            'fyos_id' => 'Fyos ID',
            'fyos_status_id' => 'Fyos Status ID',
            'marks_obtained' => 'Marks Obtained',
        ];
    }
}
