<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Login;
use kartik\password\StrengthValidator;

class PasswordForm extends Model {

    public $oldpass;
    public $newpass;
    public $repeatnewpass;
    public $username;

    public function rules() {
        return [
            [['oldpass', 'newpass', 'repeatnewpass'], 'required'],
            ['oldpass', 'findPasswords'],
            [['repeatnewpass', 'newpass'], StrengthValidator::className(), 'preset' => 'normal', 'userAttribute' => 'username'],
            ['repeatnewpass', 'compare', 'compareAttribute' => 'newpass', 'message' => "Passwords don't match"],
        ];
    }

    public function findPasswords($attribute, $params) {
        $user = Users::find()->where([
                    'user_id' => Yii::$app->user->identity->id])->one();
        $hash = $user->password;
        //$password = $user->password;
        //if ($password != (sha1($this->oldpass))) {
        if ((Yii::$app->getSecurity()->validatePassword($this->oldpass, $hash)) != TRUE) {
              Yii::$app->getSession()->setFlash(
                                    'erroroldpwd', 'Please enter your collect old password!'
                            );
            $this->addError($attribute, 'Old password is incorrect');
        }
    }

    public function attributeLabels() {
        return [
            'oldpass' => 'Old Password',
            'newpass' => 'New Password',
            'repeatnewpass' => 'Repeat New Password',
        ];
    }

}
