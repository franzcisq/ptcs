<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StreetsVillage;

/**
 * StreetsVillageSearch represents the model behind the search form about `app\models\StreetsVillage`.
 */
class StreetsVillageSearch extends StreetsVillage
{
    public function rules()
    {
        return [
            [['street_id', 'ward_id', 'is_active'], 'integer'],
            [['street'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StreetsVillage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'street_id' => $this->street_id,
            'ward_id' => $this->ward_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'street', $this->street]);

        return $dataProvider;
    }
}
