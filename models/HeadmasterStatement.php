<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "headmaster_statement".
 *
 * @property integer $headmaster_statement_id
 * @property integer $school_id
 * @property integer $user_id
 * @property string $statement
 * @property string $date_created
 * @property integer $is_active
 */
class HeadmasterStatement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'headmaster_statement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'user_id', 'statement', 'is_active'], 'required'],
            [['school_id', 'user_id', 'is_active'], 'integer'],
            [['statement'], 'string'],
            [['date_created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'headmaster_statement_id' => 'Headmaster Statement ID',
            'school_id' => 'School ID',
            'user_id' => 'User ID',
            'statement' => 'Statement',
            'date_created' => 'Date Created',
            'is_active' => 'Is Active',
        ];
    }
}
