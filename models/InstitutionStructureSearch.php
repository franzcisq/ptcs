<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InstitutionStructure;

/**
 * InstitutionStructureSearch represents the model behind the search form about `app\models\InstitutionStructure`.
 */
class InstitutionStructureSearch extends InstitutionStructure
{
    public function rules()
    {
        return [
            [['institution_structure_id', 'parent_institution_structure_id', 'institution_type_id'], 'integer'],
            [['institution_name', 'institution_acronym'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = InstitutionStructure::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'institution_structure_id' => $this->institution_structure_id,
            'parent_institution_structure_id' => $this->parent_institution_structure_id,
            'institution_type_id' => $this->institution_type_id,
        ]);

        $query->andFilterWhere(['like', 'institution_name', $this->institution_name])
            ->andFilterWhere(['like', 'institution_acronym', $this->institution_acronym]);

        return $dataProvider;
    }
}
