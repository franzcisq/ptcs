<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClassYearOfStudy;

/**
 * ClassYearOfStudySearch represents the model behind the search form about `app\models\ClassYearOfStudy`.
 */
class ClassYearOfStudySearch extends ClassYearOfStudy
{
    public function rules()
    {
        return [
            [['cyos_id', 'cy_id', 'cyos_number', 'cyos_status_id', 'approval_status_id', 'publish_status_id'], 'integer'],
            [['cyos_description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ClassYearOfStudy::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'cyos_id' => $this->cyos_id,
            'cy_id' => $this->cy_id,
            'cyos_number' => $this->cyos_number,
            'cyos_status_id' => $this->cyos_status_id,
            'approval_status_id' => $this->approval_status_id,
            'publish_status_id' => $this->publish_status_id,
        ]);

        $query->andFilterWhere(['like', 'cyos_description', $this->cyos_description]);

        return $dataProvider;
    }
}
