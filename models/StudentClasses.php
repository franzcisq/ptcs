<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_classes".
 *
 * @property integer $sc_id
 * @property integer $student_year_of_study_id
 * @property integer $cyos_id
 * @property integer $cyos_status_id
 * @property double $marks_obtained
 */
class StudentClasses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_classes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_year_of_study_id', 'cyos_id', 'cyos_status_id', 'marks_obtained'], 'required'],
            [['student_year_of_study_id', 'cyos_id', 'cyos_status_id'], 'integer'],
            [['marks_obtained'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sc_id' => 'Sc ID',
            'student_year_of_study_id' => 'Student Year Of Study ID',
            'cyos_id' => 'Cyos ID',
            'cyos_status_id' => 'Cyos Status ID',
            'marks_obtained' => 'Marks Obtained',
        ];
    }
}
