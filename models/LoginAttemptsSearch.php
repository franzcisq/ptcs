<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LoginAttempts;

/**
 * LoginAttemptsSearch represents the model behind the search form about `app\models\LoginAttempts`.
 */
class LoginAttemptsSearch extends LoginAttempts
{
    public function rules()
    {
        return [
            [['login_attempts_id', 'user_id', 'successfully_attempt'], 'integer'],
            [['ip_address', 'last_login'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = LoginAttempts::find();
        $query->orderBy("login_attempts_id DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'login_attempts_id' => $this->login_attempts_id,
            'user_id' => $this->user_id,
            'successfully_attempt' => $this->successfully_attempt,
            'last_login' => $this->last_login,
        ]);

        $query->andFilterWhere(['like', 'ip_address', $this->ip_address]);

        return $dataProvider;
    }
}
