<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormYear;

/**
 * FormYearSearch represents the model behind the search form about `app\models\FormYear`.
 */
class FormYearSearch extends FormYear
{
    public function rules()
    {
        return [
            [['fy_id', 'academic_year_id', 'school_id', 'year_number', 'grading_system_version_id', 'study_year', 'required_year_units', 'form_year_status_id', 'approval_status_id', 'publish_status_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = FormYear::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fy_id' => $this->fy_id,
            'academic_year_id' => $this->academic_year_id,
            'school_id' => $this->school_id,
            'year_number' => $this->year_number,
            'grading_system_version_id' => $this->grading_system_version_id,
            'study_year' => $this->study_year,
            'required_year_units' => $this->required_year_units,
            'form_year_status_id' => $this->form_year_status_id,
            'approval_status_id' => $this->approval_status_id,
            'publish_status_id' => $this->publish_status_id,
        ]);

        return $dataProvider;
    }
}
