<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Login;
use kartik\password\StrengthValidator;

class DefaultpasswdForm extends Model {

   
    public $newpass;
    public $repeatnewpass;


    public function rules() {
        return [
            [[ 'newpass', 'repeatnewpass'], 'required'],
            ['newpass', 'findPasswords'],
            ['repeatnewpass', 'compare', 'compareAttribute' => 'newpass', 'message' => "Passwords don't match"],
        ];
    }

    public function findPasswords($attribute, $params) {
        $user = Users::find()->where([
                    'user_id' => Yii::$app->user->identity->id])->one();
          $hash = $user->password;
        if ((Yii::$app->getSecurity()->validatePassword($this->newpass, $hash)) != TRUE) {
              Yii::$app->getSession()->setFlash(
                                    'errorMessage', 'Please enter new password!'
                            );
            $this->addError($attribute, 'New Password can not be the same as default password!');
        }
    }

    public function attributeLabels() {
        return [
          
            'newpass' => 'New Password',
            'repeatnewpass' => 'Repeat New Password',
        ];
    }

}
