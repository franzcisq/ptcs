<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Logins;

/**
 * LoginsSearch represents the model behind the search form about `app\models\Logins`.
 */
class LoginsSearch extends Logins
{
    public function rules()
    {
        return [
            [['login_id', 'user_id'], 'integer'],
            [['ip_address', 'details', 'datecreated'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $condition = "1=1")
    {
        $query = Logins::find();
        $query->andWhere($condition);
        $query->orderBy(" login_id DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'login_id' => $this->login_id,
            'user_id' => $this->user_id,
            'datecreated' => $this->datecreated,
        ]);

        $query->andFilterWhere(['like', 'ip_address', $this->ip_address])
            ->andFilterWhere(['like', 'details', $this->details]);

        return $dataProvider;
    }
}
