<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormYearOfStudyCourses;

/**
 * FormYearOfStudyCoursesSearch represents the model behind the search form about `app\models\FormYearOfStudyCourses`.
 */
class FormYearOfStudyCoursesSearch extends FormYearOfStudyCourses
{
    public function rules()
    {
        return [
            [['fyos_course_id', 'fy_id', 'school_id', 'course_offering_id', 'pass_grade_id', 'contribute_to_final_result', 'is_core'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = FormYearOfStudyCourses::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'fyos_course_id' => $this->fyos_course_id,
            'fy_id' => $this->fy_id,
            'school_id' => $this->school_id,
            'course_offering_id' => $this->course_offering_id,
            'pass_grade_id' => $this->pass_grade_id,
            'contribute_to_final_result' => $this->contribute_to_final_result,
            'is_core' => $this->is_core,
        ]);

        return $dataProvider;
    }
}
