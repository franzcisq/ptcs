<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_status".
 *
 * @property string $student_status_id
 * @property string $student_status
 */
class StudentStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_status'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_status_id' => 'Student Status ID',
            'student_status' => 'Student Status',
        ];
    }
}
