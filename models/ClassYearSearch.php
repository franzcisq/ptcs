<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClassYear;

/**
 * ClassYearSearch represents the model behind the search form about `app\models\ClassYear`.
 */
class ClassYearSearch extends ClassYear
{
    public function rules()
    {
        return [
            [['cy_id', 'academic_year_id', 'year_number', 'grading_system_version_id', 'study_year', 'required_year_units', 'class_year_status_id', 'approval_status_id', 'publish_status_id', 'school_id'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ClassYear::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'cy_id' => $this->cy_id,
            'academic_year_id' => $this->academic_year_id,
            'year_number' => $this->year_number,
            'grading_system_version_id' => $this->grading_system_version_id,
            'study_year' => $this->study_year,
            'required_year_units' => $this->required_year_units,
            'class_year_status_id' => $this->class_year_status_id,
            'approval_status_id' => $this->approval_status_id,
            'publish_status_id' => $this->publish_status_id,
            'school_id' => $this->school_id,
        ]);

        return $dataProvider;
    }
}
