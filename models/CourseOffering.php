<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "course_offering".
 *
 * @property integer $course_offering_id
 * @property string $admistered_by
 * @property string $course_id
 * @property integer $course_offering_version_id
 *
 * @property ClassYearOfStudyCourses[] $classYearOfStudyCourses
 * @property CourseOfferingVersions $courseOfferingVersion
 */
class CourseOffering extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_offering';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admistered_by', 'course_id', 'course_offering_version_id'], 'integer'],
            [['course_offering_version_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'course_offering_id' => 'Course Offering ID',
            'admistered_by' => 'Admistered By',
            'course_id' => 'Course',
            'course_offering_version_id' => 'Course Offering Version ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassYearOfStudyCourses()
    {
        return $this->hasMany(ClassYearOfStudyCourses::className(), ['course_offering_id' => 'course_offering_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseOfferingVersion()
    {
        return $this->hasOne(CourseOfferingVersions::className(), ['course_offering_version_id' => 'course_offering_version_id']);
    }
}
