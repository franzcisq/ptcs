<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StaffPositions;

/**
 * StaffPositionsSearch represents the model behind the search form about `app\models\StaffPositions`.
 */
class StaffPositionsSearch extends StaffPositions
{
    public function rules()
    {
        return [
            [['staff_position_id'], 'integer'],
            [['position_name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StaffPositions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'staff_position_id' => $this->staff_position_id,
        ]);

        $query->andFilterWhere(['like', 'position_name', $this->position_name]);

        return $dataProvider;
    }
}
