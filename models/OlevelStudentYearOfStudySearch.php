<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OlevelStudentYearOfStudy;

/**
 * OlevelStudentYearOfStudySearch represents the model behind the search form about `app\models\OlevelStudentYearOfStudy`.
 */
class OlevelStudentYearOfStudySearch extends OlevelStudentYearOfStudy {

    public function rules() {
        return [
            [['olevel_student_year_of_study_id', 'user_id', 'fy_id', 'student_year_status_id', 'year_of_study', 'year_units', 'status'], 'integer'],
            [['results', 'year_grade_points'], 'number'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $condition = "1=1") {
        $query = OlevelStudentYearOfStudy::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'olevel_student_year_of_study_id' => $this->olevel_student_year_of_study_id,
            'user_id' => $this->user_id,
            'fy_id' => $this->fy_id,
            'student_year_status_id' => $this->student_year_status_id,
            'year_of_study' => $this->year_of_study,
            'results' => $this->results,
            'year_units' => $this->year_units,
            'year_grade_points' => $this->year_grade_points,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }

}
