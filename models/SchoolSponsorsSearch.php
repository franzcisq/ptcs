<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SchoolSponsors;

/**
 * SchoolSponsorsSearch represents the model behind the search form about `app\models\SchoolSponsors`.
 */
class SchoolSponsorsSearch extends SchoolSponsors
{
    public function rules()
    {
        return [
            [['school_sponsor_id', 'school_id', 'sponsor_id', 'is_visible'], 'integer'],
            [['what_sponsored', 'amount', 'date_created', 'date_sponsored'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = SchoolSponsors::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'school_sponsor_id' => $this->school_sponsor_id,
            'school_id' => $this->school_id,
            'sponsor_id' => $this->sponsor_id,
            'date_created' => $this->date_created,
            'date_sponsored' => $this->date_sponsored,
            'is_visible' => $this->is_visible,
        ]);

        $query->andFilterWhere(['like', 'what_sponsored', $this->what_sponsored])
            ->andFilterWhere(['like', 'amount', $this->amount]);

        return $dataProvider;
    }
}
