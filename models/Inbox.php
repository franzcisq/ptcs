<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inbox".
 *
 * @property integer $inbox_id
 * @property integer $message_id
 * @property integer $sent_by
 * @property integer $received_by
 * @property integer $school_id
 * @property integer $status
 */
class Inbox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'sent_by', 'received_by', 'school_id', 'status'], 'required'],
            [['message_id', 'sent_by', 'received_by', 'school_id', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'inbox_id' => 'Inbox ID',
            'message_id' => 'Message ID',
            'sent_by' => 'Sent By',
            'received_by' => 'Received By',
            'school_id' => 'School ID',
            'status' => 'Status',
        ];
    }
}
