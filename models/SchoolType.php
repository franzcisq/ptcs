<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school_type".
 *
 * @property integer $school_type_id
 * @property string $school_type
 * @property integer $is_active
 */
class SchoolType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_type', 'is_active'], 'required'],
            [['is_active'], 'integer'],
            [['school_type'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_type_id' => 'School Type ID',
            'school_type' => 'School Type',
            'is_active' => 'Is Active',
        ];
    }
}
