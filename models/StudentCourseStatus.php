<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_course_status".
 *
 * @property integer $student_course_status_id
 * @property string $course_status
 * @property string $abbreviation
 */
class StudentCourseStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_course_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_course_status_id', 'course_status', 'abbreviation'], 'required'],
            [['student_course_status_id'], 'integer'],
            [['course_status'], 'string', 'max' => 30],
            [['abbreviation'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_course_status_id' => 'Student Course Status ID',
            'course_status' => 'Course Status',
            'abbreviation' => 'Abbreviation',
        ];
    }
}
