<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "board_members".
 *
 * @property integer $board_members_id
 * @property integer $school_board_id
 * @property integer $user_id
 * @property integer $is_active
 */
class BoardMembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'board_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_board_id', 'user_id', 'is_active'], 'required'],
            [['school_board_id', 'user_id', 'is_active'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'board_members_id' => 'Board Members ID',
            'school_board_id' => 'School Board ID',
            'user_id' => 'User ID',
            'is_active' => 'Is Active',
        ];
    }
}
