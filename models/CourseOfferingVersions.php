<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "course_offering_versions".
 *
 * @property integer $course_offering_version_id
 * @property string $description
 * @property integer $status
 *
 * @property CourseOffering[] $courseOfferings
 */
class CourseOfferingVersions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_offering_versions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'status'], 'required'],
            [['status','school_type_id'], 'integer'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'course_offering_version_id' => 'Course Offering Version ID',
            'description' => 'Description',
            'school_type_id'=>'school Type',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseOfferings()
    {
        return $this->hasMany(CourseOffering::className(), ['course_offering_version_id' => 'course_offering_version_id']);
    }
}
