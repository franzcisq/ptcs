<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SchoolDonors;

/**
 * SchoolDonorsSearch represents the model behind the search form about `app\models\SchoolDonors`.
 */
class SchoolDonorsSearch extends SchoolDonors
{
    public function rules()
    {
        return [
            [['school_donor_id', 'school_id', 'donor_id', 'is_visible'], 'integer'],
            [['what_donated', 'amount', 'date_created', 'date_donated'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition)
    {
        $query = SchoolDonors::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'school_donor_id' => $this->school_donor_id,
            'school_id' => $this->school_id,
            'donor_id' => $this->donor_id,
            'date_created' => $this->date_created,
            'date_donated' => $this->date_donated,
            'is_visible' => $this->is_visible,
        ]);

        $query->andFilterWhere(['like', 'what_donated', $this->what_donated])
            ->andFilterWhere(['like', 'amount', $this->amount]);

        return $dataProvider;
    }
}
