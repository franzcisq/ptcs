<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use kartik\widgets\FileInput;
use app\models\PasswordForm;
use app\models\Login;
use app\models\LoginForm;
use yii\base\Security;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UsersController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete', 'update', 'profile', 'updatepic', 'updateidentification', 'passwordreset', 'changepassword', 'deactivate', 'students'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex($id_to_deactivate = NULL, $id_to_activate = NULL, $subaction = NULL) {
        $searchModel = new UsersSearch;
        $condition = "user_type = 2";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);

        if ($subaction == 'deactivate') {
            $model_user = Users::findOne($id_to_deactivate);
            $model_user->is_active = 0;
            if ($model_user->save(false)) { 
        $objectType = 'Users';
           $action = "Deactivated User, user_id => ".' '.$id_to_deactivate;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                return $this->render('index', [
                            'dataProvider' => $dataProvider,
                            'searchModel' => $searchModel,
                ]);
            }
        }

        if ($subaction == 'activate') {
            $model_user = Users::findOne($id_to_activate);
            $model_user->is_active = 1;
            if ($model_user->save(false)) {
        $objectType = 'Users';
           $action = "Activated User, user_id => ".' '.$id_to_activate;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                return $this->render('index', [
                            'dataProvider' => $dataProvider,
                            'searchModel' => $searchModel,
                ]);
            }
        }
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionProfile() {
        $model = Users::findOne(\yii::$app->user->identity->id);
        $email = $model->email_address;
        $phone_no = $model->telephone_no;
        if ($email == NULL || $phone_no == NULL) {
            Yii::$app->getSession()->setFlash(
                    'success', 'Please update your identification information including your E-mail Address & Mobile Phone Number before continuing...!'
            );
        }
        return $this->render('profile', [
                    'model' => $model,
        ]);
    }

    public function actionUpdatepic($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            $model->image = UploadedFile::getInstance($model, 'image');
            if (($model->image) != NULL) {
                $filepath = 'uploads/profilepicture/' . md5($model->user_id) . '.' . $model->image->extension;
                $model->profile_picture = $filepath;

                if ($model->save(false)) {
                    if ($model->image->saveAs($filepath)) {
          $objectType = 'Users';
             $action = "Updated his/her profile photo";
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                        Yii::$app->getSession()->setFlash(
                                'success', 'Profile Photo Successfuly Updated!'
                        );

                        return $this->redirect(['profile', 'id' => $model->user_id]);
                    }
                }
            } else {
                return $this->redirect(['profile', 'id' => $model->user_id]);
            }
        } else {
            return $this->render('updatepic', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdateidentification($id) {
        $model = $this->findModel($id);
//        $model->scenario = 'updateidentification';
        if ($model->load(Yii::$app->request->post())) {
            if ($model->email_address == NULL) {
                $model->addError('email_address', 'Provide Your E-mail Address');
                return $this->render('updateidentification', [
                            'model' => $model,
                ]);
            }

            if ($model->save(false)) {
        $objectType = 'Users';
             $action = "Updated his/her Identification Information";
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                Yii::$app->getSession()->setFlash(
                        'success', 'Identifications Successfuly updated!'
                );
                return $this->redirect(['profile', 'id' => $model->user_id]);
            }
        } else {
            return $this->render('updateidentification', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);

//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->user_id]);
//        } 
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->user_id]);
            }
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Users;
        if ($model->load(Yii::$app->request->post())) {
            $password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            $model->password = $password;
            $model->user_type = 2;
            $model->is_default_password = 1;
            if ($model->save(false)) {
           $objectType = 'Users';
             $action = "Added User (Staff)".' '.'staff_id => '.' '.$model->user_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                return $this->redirect(['view', 'id' => $model->user_id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
             $oldModel = $model;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
        $objectType = 'User';
          $action = "Updated User ";
                \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
                return $this->redirect(['view', 'id' => $model->user_id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionPasswordreset($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $password = Yii::$app->getSecurity()->generatePasswordHash($model->newpass);
            $model->password = $password;
            $model->is_default_password = 1;
            if ($model->save(false)) {
         $objectType = 'Users';
             $action = "Changed his/her password".' '.'staff_id => '.' '.$model->user_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                Yii::$app->getSession()->setFlash(
                        'success', 'Password Successfully updated!'
                );
                return $this->redirect(['view', 'id' => $model->user_id]);
            }
        } else {
            return $this->render('passwordreset', [
                        'model' => $model,
                        'id' => $model->user_id,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
     $model=  $this->findModel($id);
        $this->findModel($id)->delete();
           $objectType = 'User';
           $action = "Deleted User (staff)".' '."staff_id =>".$model->user_id;
             \app\models\UserAuditTrail::logAudit($action, $objectType);

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangepassword() {
        $model = new PasswordForm;
        $modeluser = Users::find()->where([
                    'user_id' => Yii::$app->user->identity->id
                ])->one();
        $oldpasswd = $modeluser->password;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                try {
                    $hash = Yii::$app->getSecurity()->generatePasswordHash($_POST['PasswordForm']['newpass']);
                    if ((Yii::$app->getSecurity()->validatePassword($model->newpass, $oldpasswd)) != TRUE) {
                        $modeluser->password = $hash;
                        $modeluser->is_default_password = 0;
                        if ($modeluser->save(false)) {
           $objectType = 'User';
           $action = "Changed Password";
             \app\models\UserAuditTrail::logAudit($action, $objectType);
                            Yii::$app->getSession()->setFlash(
                                    'success', 'Password Successfuly changed!'
                            );
                            return $this->redirect(['profile', 'id' => $modeluser->user_id]);
                        } else {
                            Yii::$app->getSession()->setFlash(
                                    'error', 'Password not changed!'
                            );
//                        var_dump($modeluser->errors);
//                         die();
                            return $this->redirect(['profile', 'id' => $modeluser->user_id]);
                        }
                    } else {
                        Yii::$app->getSession()->setFlash(
                                'errorMessage', 'Please enter new password...!'
                        );
                        $model->addError('newpass', 'New Password can not be the same as older password!');
                        return $this->render('changepassword', [
                                    'model' => $model
                        ]);
                    }
                } catch (Exception $e) {
                    Yii::$app->getSession()->setFlash(
                            'error', "{$e->getMessage()}"
                    );
                    return $this->render('changepassword', [
                                'model' => $model
                    ]);
                }
            } else {
                return $this->render('changepassword', [
                            'model' => $model
                ]);
            }
        } else {
            return $this->render('changepassword', [
                        'model' => $model
            ]);
        }
    }

}
