<?php

namespace app\controllers;

use yii\filters\AccessControl;
use Yii;
use app\models\AuthAssignment;
use app\models\AuthAssignmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * AuthAssignmentController implements the CRUD actions for AuthAssignment model.
 */
class AuthAssignmentController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthAssignment models.
     * @return mixed
     */
    public function actionIndex($user_id) {
        $this->layout = 'simple_layout';
//        if (!Yii::$app->user->can('/auth-assignment/index')) {
//            throw new ForbiddenHttpException('Your not allowed to perform this action ! Contact your administrator');
//        }

        $assignmentModel = new AuthAssignment();
        $searchModel = new AuthAssignmentSearch;

        if (isset($_POST['AuthAssignment'])) {
            $assignmentModel->load(Yii::$app->request->post());
            $assignmentModel->user_id = $user_id;

            $validateAssigment = AuthAssignment::find()->where(['item_name' => $assignmentModel->item_name, 'user_id' => $assignmentModel->user_id])->asArray()->all();

            if ($validateAssigment) {
                //$assignmentModel->addError("institution_structure_id", "The user has this level already ");
                $assignmentModel->addError("item_name", "The user has this role already ");
            }
            if ($assignmentModel->validate(null, false)) {
                if ($assignmentModel->save()) {
                    $level_based_routes = \app\models\Users::getItemRoutesWithLevelBased($assignmentModel->item_name);
                    foreach ($level_based_routes as $route) {
                        if (\app\models\AuthAssignedRoute::find()->where("auth_item_name = '{$route}' AND user_id = {$user_id} ")->count() == 0) {
                            $modelAuthAssigned = new \app\models\AuthAssignedRoute();
                            $modelAuthAssigned->user_id = $user_id;
                            $modelAuthAssigned->auth_item_name = $route;
                            $modelAuthAssigned->description = \app\models\AuthItem::find()->where("name = '{$route}'")->one()->description;
                            $modelAuthAssigned->save();
                        }
                    }
                    //removing routes that are no longer required
                    $level_based_routes_all = \app\models\Users::getUserRoutesWithLevelBased($user_id);
                    $existing_user_routes = \app\models\AuthAssignedRoute::find()->where("user_id = {$user_id}");
                    foreach ($existing_user_routes as $route) {
                        if (!in_array($route, $level_based_routes_all)) {
                            \app\models\AuthAssignedRoute::deleteAll("user_id = {$user_id} AND auth_item_name = '{$route}'");
                        }
                    }

                    $objectType = 'AuthAssignment';
                    $action = "Asssigned  Authentication" . ' ' . 'item_name =>' . ' ' . $assignmentModel->item_name . ' ' . 'user_id =>' . $assignmentModel->user_id . ' ' . 'institution_structure_id => ' . ' ' . $assignmentModel->institution_structure_id;
                    \app\models\UserAuditTrail::logAudit($action, $objectType);
                    $assignmentModel = new AuthAssignment();
                }
            }
        }
        $condition = "user_id = '$user_id'";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'assignmentModel' => $assignmentModel,
                    'user_id' => $user_id,
                        //'user_details' => $user_details,
        ]);
    }

    /**
     * Displays a single AuthAssignment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//
//        if (!Yii::$app->user->can('Administrator')) {
//            throw new ForbiddenHttpException('Your not allowed to perform this action ! Contact your administrator');
//        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->assignment_id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new AuthAssignment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

//        if (!Yii::$app->user->can('Administrator')) {
//            throw new ForbiddenHttpException('Your not allowed to perform this action ! Contact your administrator');
//        }

        $model = new AuthAssignment;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $objectType = 'AuthAssignment';
            $action = "Created AuthAssignment" . ' ' . $model->item_name . ' ' . "assignment_id =>" . $model->assignment_id;
            \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['view', 'id' => $model->assignment_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthAssignment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

//        if (!Yii::$app->user->can('Administrator')) {
//            throw new ForbiddenHttpException('Your not allowed to perform this action ! Contact your administrator');
//        }

        $model = $this->findModel($id);
        $oldModel = $model;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $objectType = 'AuthAssignment';
            $action = "Updated AuthAssignment ";
            \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
            return $this->redirect(['view', 'id' => $model->assignment_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthAssignment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $user_id) {
//        if (!Yii::$app->user->can('Administrator')) {
//            throw new ForbiddenHttpException('Your not allowed to perform this action ! Contact your administrator');
//        }
        $model = $this->findModel($id);
        $this->findModel($id)->delete();

        //removing routes that are no longer required
        $level_based_routes_all = \app\models\Users::getUserRoutesWithLevelBased($user_id);
        $existing_user_routes = \app\models\AuthAssignedRoute::find()->where("user_id = {$user_id}")->all();
        foreach ($existing_user_routes as $route) {
            if (!in_array($route->auth_item_name, $level_based_routes_all)) {
                \app\models\AuthAssignedRoute::deleteAll("user_id = {$user_id} AND auth_item_name = '{$route->auth_item_name}'");
            }
        }

        $objectType = 'AuthAssignment';
        $action = 'Deleted ' . ' ' . 'Role =>' . ' ' . $model->item_name . ' ' . 'From' . ' ' . 'User_id =>' . ' ' . $user_id . ' ' . 'user_details =>' . $user_details;
        \app\models\UserAuditTrail::logAudit($action, $objectType);
        return $this->redirect(['index', 'user_id' => $user_id, 'user_details' => $user_details]);
    }

    /**
     * Finds the AuthAssignment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AuthAssignment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthAssignment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
