<?php

namespace app\controllers;

use Yii;
use app\models\AuthAssignedRoute;
use app\models\AuthAssignedRouteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * AuthAssignedRouteController implements the CRUD actions for AuthAssignedRoute model.
 */
class AuthAssignedRouteController extends Controller {

    public function behaviors() {
        return [
               'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete', 'update', 'approve', 'results', 'semesters', 'approve-publish-results'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthAssignedRoute models.
     * @return mixed
     */
    public function actionIndex($user_id) {
//         if (!Yii::$app->user->can('/auth-assigned-route/index')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        if (Yii::$app->request->post('hasEditable') && isset($_POST['AuthAssignedRoute'])) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $message = '';
            $out = '';
            $record_id = Yii::$app->request->post('editableKey');
            $model1 = \app\models\AuthAssignedRoute::findOne($record_id);
            $oldModel = $model1;
            $post = [];
            $posted = current($_POST['AuthAssignedRoute']);
            $post['AuthAssignedRoute'] = $posted;

            if ($model1->load($post)) {
                if (!$model1->save(false)) {
                    $message = 'Error occured';
                } else {

                    $objectType = 'AuthAssignedRoute';
                    $action = "Updated User Assigened Route ";
                    \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model1);
                    $modelIns = \app\models\InstitutionStructure::findOne($model1->institution_structure_id);
                    $out = $modelIns->institution_name ." [".\app\models\InstitutionType::findOne($modelIns->institution_type_id)->institution_type_name."]";
                }
            } else {
                $message = 'Error occured';
            }
            return ['output' => $out, 'message' => $message];
        }


        $this->layout = 'simple_layout';
        $searchModel = new AuthAssignedRouteSearch;
        $condition = "user_id = {$user_id}";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single AuthAssignedRoute model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//          if (!Yii::$app->user->can('/auth-assigned-route/view')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->auth_assigned_route_id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new AuthAssignedRoute model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
//          if (!Yii::$app->user->can('/auth-assigned-route/create')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model = new AuthAssignedRoute;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->auth_assigned_route_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthAssignedRoute model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
      if (!Yii::$app->user->can('/auth-assigned-route/update')) {
             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->auth_assigned_route_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthAssignedRoute model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthAssignedRoute model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AuthAssignedRoute the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AuthAssignedRoute::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
