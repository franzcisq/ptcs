<?php

namespace app\controllers;

use Yii;
use app\models\InstitutionType;
use app\models\InstitutionTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * InstitutionTypeController implements the CRUD actions for InstitutionType model.
 */
class InstitutionTypeController extends Controller
{
    public function behaviors()
    {
        return [
                'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','delete','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InstitutionType models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        if (!Yii::$app->user->can('/institution-type/index')) {
             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
        }
        
        $searchModel = new InstitutionTypeSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single InstitutionType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
       if (!Yii::$app->user->can('/institution-type/view')) {
             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->institution_type_id]);
        } else {
        return $this->render('view', ['model' => $model]);
}
    }

    /**
     * Creates a new InstitutionType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         if (!Yii::$app->user->can('/institution-type/create')) {
             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
        }
        
        $model = new InstitutionType;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           $objectType = 'InstitutionType';
           $action = "Created InstitutionType".' '.$model->institution_type_name.' '."institution_type_id =>".$model->institution_type_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['index', 'id' => $model->institution_type_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing InstitutionType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('/institution-type/update')) {
             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
        }
        $model = $this->findModel($id);
         $oldModel = $model;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          $objectType = 'InstitutionType';
          $action = "Updated InstitutionType ";
                \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
            return $this->redirect(['index', 'id' => $model->institution_type_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InstitutionType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
    if (!Yii::$app->user->can('/institution-type/delete')) {
             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
        }
          $model=  $this->findModel($id);
        $this->findModel($id)->delete();
           $objectType = 'InstitutionType';
           $action = "Deleted InstitutionType".' '.$model->institution_type_name.' '."institution_type_id =>".$model->institution_type_id;
             \app\models\UserAuditTrail::logAudit($action, $objectType);
        return $this->redirect(['index']);
    }

    /**
     * Finds the InstitutionType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InstitutionType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InstitutionType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
