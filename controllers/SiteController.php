<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;
use app\models\PasswordForm;
use yii\helpers\Url;
use app\models\Students;
use yii\helpers\Html;
use app\controllers\StudentsController;
use app\models\DefaultpasswdForm;

date_default_timezone_set('Africa/Nairobi');

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {

        if (!\Yii::$app->user->isGuest) {
            $id = Yii::$app->user->identity->id;
            $modeluser = Users::find()->where([
                        'user_id' => $id
                    ])->one();
            $default_password = $modeluser->is_default_password;
            $oldpasswd = $modeluser->password;
            if ($default_password == 1) {
                $model = new DefaultpasswdForm();
                if ($model->load(Yii::$app->request->post())) {

                    if ((Yii::$app->getSecurity()->validatePassword($model->newpass, $oldpasswd)) != TRUE) {

                        $new_password_hash = Yii::$app->getSecurity()->generatePasswordHash($model->newpass);
                        $modeluser->password = $new_password_hash;
                        $modeluser->is_default_password = 0;
                        if ($modeluser->save(false)) {
                            return $this->redirect(['/site/login']);
                        } else {
                            return $this->render('defaultpassword', ['model' => $model]);
                        }
                    } else {

                        \Yii::$app->session->setFlash('errorMessage', 'Please enter new password!');
                        $model->addError('newpass', 'New Password can not be the same as default password!');
                        return $this->render('defaultpassword', ['model' => $model]);
                    }
                } else {
                    return $this->render('defaultpassword', ['model' => $model]);
                }
            } else {
                return $this->render('index');
            }
        } else {
            return $this->redirect(['/site/login']);
        }
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            /* Logs the Logins History */
            $loginsModel = new \app\models\Logins();
            $loginsModel->user_id = \yii::$app->user->identity->id;
            $loginsModel->ip_address = Yii::$app->getRequest()->getUserIP();
            $loginsModel->details = 'User logged into the system successful using browser :- ' . Yii::$app->getRequest()->getUserAgent();
            $loginsModel->save();
            return $this->goBack();
        } else { 
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        $userId = \yii::$app->user->identity->id;
        Yii::$app->user->logout();
        /* Logs the Logins History */
        $loginsModel = new \app\models\Logins();
        $loginsModel->user_id = $userId;
        $loginsModel->ip_address = Yii::$app->getRequest()->getUserIP();
        $loginsModel->details = 'User logged out the system successful using browser :- ' . Yii::$app->getRequest()->getUserAgent();
        $loginsModel->save();
        return $this->goHome();
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    public function actionAbout() {
        return $this->render('about');
    }

    public function actionReset() {

        $model = new \app\models\ResetPassword();
        if ($model->load(Yii::$app->request->post())) {
            $email = trim($model->E_mail);
            $phone_number = $model->Phone_number;
            $user_model = Users::find()->where("telephone_no = :phone_no  AND email_address = :email", ['phone_no' => $phone_number, 'email' => $email])->one();
            $id = $user_model->user_id;

            if ($id != NULL) {
                $secretKey = $model->Phone_number;
                $encryptedData = Yii::$app->getSecurity()->encryptByPassword($id, $secretKey);

                $message = $message . ' ' . 'Click this link to reset your Aris password! ' . \yii\helpers\Html::a('Reset', Yii::$app->urlManager->createAbsoluteUrl(
                                        ['site/passwordreset', 'verification' => $encryptedData, 'phone' => $phone_number]
                ));
                $subject = 'Aris: Reset Your Password';
                $mail = Yii::$app->mail->compose()
                        ->setFrom('query@aris.com')
                        ->setTo($model->E_mail)
                        ->setSubject($subject)
                        ->setHtmlBody($message)
                        ->send();

                if ($mail) {
                    \Yii::$app->session->setFlash('flashMessage', 'Check your E-mail!');
                    return $this->redirect(['/site/login']);
                } else {
                    \Yii::$app->session->setFlash('errorMessage', 'Enter correct detail and try again!');
                    $model->addError('Phone_number', 'Error Occured! Confirmation not sent!');
                    return $this->render('reset', [
                                'model' => $model,
                    ]);
                }
            } else {
                \Yii::$app->session->setFlash('failMessage', 'Please Contact Administrator!');
                $model->addError('Phone_number', 'Incorect E-mail Address or Mobile Phone Number!');
                return $this->render('reset', [
                            'model' => $model,
                ]);
            }
        }

        return $this->render('reset', [
                    'model' => $model,
        ]);
    }

    public function actionPasswordreset($verification, $phone) {
        $secretKey = $phone;
        $data = Yii::$app->getSecurity()->decryptByPassword($verification, $secretKey);
        $model = Users::findOne($data);
        if ($model->load(Yii::$app->request->post())) {
            $password = Yii::$app->getSecurity()->generatePasswordHash($model->newpass);
            $model->password = $password;
            $model->is_default_password = 1;
            if ($model->save(false)) {
             $objectType = 'ResetPassword';
             $action = " Changed Password";
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                $date_time = date("l jS  F Y h:i:sa ");
                $subject = 'Aris: Reset Your Password';
                $message = 'Hi,' . $model->firstname . ' ' . $model->middlename . ' ' . $model->surname . ' ' . 'Your successfully changed your Aris Password! on ' . $date_time;
                Yii::$app->mail->compose()
                        ->setFrom('query@aris.com')
                        ->setTo($model->email_address)
                        ->setSubject($subject)
                        ->setTextBody($message)
                        ->send();
                \Yii::$app->session->setFlash('successMessage', ' Now login with your new Password!');
                return $this->redirect(['/site/login']);
            }
        } else {
            return $this->render('passwordreset', [
                        'model' => $model,
                        'id' => $model->user_id,
            ]);
        }
    }

}

