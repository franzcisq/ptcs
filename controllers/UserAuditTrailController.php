<?php

namespace app\controllers;

use Yii;
use app\models\UserAuditTrail;
use app\models\UserAuditTrailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserAuditTrailController implements the CRUD actions for UserAuditTrail model.
 */
class UserAuditTrailController extends Controller
{
    public function behaviors()
    {
        return [
                       'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','delete','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserAuditTrail models.
     * @return mixed
     */
    public function actionIndex()
    {
//            if (!Yii::$app->user->can('ViewAuditTrail')) {
//             throw  new ForbiddenHttpException('Your not allowed to perform this action ! Contact your administrator'); 
//        }
        
        $searchModel = new UserAuditTrailSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single UserAuditTrail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->user_audit_trail_id]);
        } else {
        return $this->render('view', ['model' => $model]);
}
    }

    /**
     * Creates a new UserAuditTrail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserAuditTrail;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
         $objectType = 'UserAuditTrail';
           $action = "Added UserAuditTrail".' '.$model->user_audit_trail_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['view', 'id' => $model->user_audit_trail_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserAuditTrail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
           $oldModel = $model;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          $objectType = 'UserAuditTrail';
          $action = "Updated UserAuditTrail ";
                \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
            return $this->redirect(['view', 'id' => $model->user_audit_trail_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserAuditTrail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=  $this->findModel($id);
        $this->findModel($id)->delete();
           $objectType = 'UserAuditTrail';
           $action = "Deleted UserAuditTrail id => ".' '.$model->user_audit_trail_id;
             \app\models\UserAuditTrail::logAudit($action, $objectType);

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserAuditTrail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAuditTrail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserAuditTrail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
