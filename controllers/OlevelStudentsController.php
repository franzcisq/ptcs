<?php

namespace app\controllers;

use Yii;
use app\models\Students;
use app\models\StudentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ProgrammesSearch;
use app\models\AcademicYearsSearch;
use yii\filters\AccessControl;
use app\models\Users;
use yii\web\ForbiddenHttpException;

/**
 * StudentsController implements the CRUD actions for Students model.
 */
class OlevelStudentsController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete', 'update', 'password-reset', 'upload-index', 'upload-ps-academic-years', 'programme-year-students', 'students-upload-excel-template'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Students models.
     * @return mixed
     */
    public function actionIndex($id_to_deactivate = NULL, $id_to_reset = NULL, $id_to_activate = NULL, $subaction = NULL) {

        if (!Yii::$app->user->can('/students/index')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }

        $searchModel = new StudentsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        if ($subaction == 'deactivate') {
            $model_user = Users::findOne($id_to_deactivate);
            $model_user->is_active = 0;
            if ($model_user->save(false)) {
                $objectType = 'Students';
                $action = "Deactivated Student  id => " . ' ' . $id_to_deactivate;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                return $this->render('index', [
                            'dataProvider' => $dataProvider,
                            'searchModel' => $searchModel,
                ]);
            }
        }

        if ($subaction == 'activate') {
            $model_user = Users::findOne($id_to_activate);
            $model_user->is_active = 1;
            if ($model_user->save(false)) {
                $objectType = 'Students';
                $action = "Activated Student  id => " . ' ' . $id_to_activate;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                return $this->render('index', [
                            'dataProvider' => $dataProvider,
                            'searchModel' => $searchModel,
                ]);
            }
        }


        if ($subaction == 'reset') {
            $model = $this->findModel($id_to_reset);
            if ($model->load(Yii::$app->request->post())) {
                $password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
                $model->password = $password;
                $model->is_default_password = 1;
                if ($model->save(false)) {
                    $objectType = 'Students';
                    $action = "Changed Student Password , student_id => " . ' ' . $model->user_id;
                    \app\models\UserAuditTrail::logAudit($action, $objectType);
                    Yii::$app->getSession()->setFlash(
                            'success', 'Password Successfully Changed!'
                    );
                    return $this->redirect(['view', 'id' => $model->user_id]);
                }
            } else {
                return $this->render('passwordreset', [
                            'model' => $model,
                            'id' => $model->user_id,
                ]);
            }
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionPasswordreset($id) {
        if (!Yii::$app->user->can('/students/passwordreset')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            $model->password = $password;
            $model->is_default_password = 1;
            if ($model->save(false)) {
                $objectType = 'Students';
                $action = "Changed Student Password , student_id => " . ' ' . $model->user_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                Yii::$app->getSession()->setFlash(
                        'success', 'Password Successfully Changed!'
                );
                return $this->redirect(['view', 'id' => $model->user_id]);
            }
        } else {
            return $this->render('passwordreset', [
                        'model' => $model,
                        'id' => $model->user_id,
            ]);
        }
    }
    
   

    /**
     * Displays a single Students model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        if (!Yii::$app->user->can('/students/view')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->user_id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new Students model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!Yii::$app->user->can('/students/create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = new Students;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $objectType = 'Students';
            $action = "Added Student, id => " . ' ' . $model->user_id;
            \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['view', 'id' => $model->user_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Students model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!Yii::$app->user->can('/students/update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = $this->findModel($id);
        $oldModel = $model;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $objectType = 'Students';
            $action = "Updated Student ";
            \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
            return $this->redirect(['view', 'id' => $model->user_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Students model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!Yii::$app->user->can('/students/delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = $this->findModel($id);
        $this->findModel($id)->delete();
        $objectType = 'Students';
        $action = "Deleted Student, id => " . ' ' . $model->user_id;
        \app\models\UserAuditTrail::logAudit($action, $objectType);

        return $this->redirect(['index']);
    }

    public function actionUploadIndex() {
        if (!Yii::$app->user->can('/students/upload-index')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $searchModel = new ProgrammesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('upload_index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionUploadPsAcademicYears($pintake_id) {
        if (!Yii::$app->user->can('/students/upload-ps-academic-years')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $searchModel = new AcademicYearsSearch;
        $condition = " academic_year_id IN (select academic_year_id from programme_year) ";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);

        return $this->render('upload_ps_academic_years', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'pintake_id' => $pintake_id,
        ]);
    }

    public function actionProgrammeYearStudents($py_id) {
        if (!Yii::$app->user->can('/students/programme-year-students')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = new \app\models\UploadStudents();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');


            if ($model->validate()) {

                $objPHPExcel = \PHPExcel_IOFactory::load($model->file->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $number_of_rows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

                $headerRow = 0;
                $columnPositions = array();
                $validColumns = ['REGISTRATION', 'FIRSTNAME', 'MIDDLENAME', 'SURNAME', 'SEX'];
                foreach ($sheetData as $rowData) {
                    $rowData = array_map('strtoupper', $rowData);
                    ++$headerRow;
                    if (in_array('REGISTRATION', $rowData)) {
                        foreach ($rowData as $key => $cellData) {
                            if (in_array(trim($cellData), $validColumns)) {
                                $columnPositions[trim($cellData)] = $key;
                            }
                        }
                        break;
                    }
                }

                if (count($columnPositions) < count($validColumns)) {
                    $model->addError("file", "Invalid Columns. Required: " . implode(",", $validColumns));
                }

                if ($model->validate(NULL, false)) {




                    $uploadReport = '
                                <table border = "0">
                <thead>
                <tr>
                <th>SN</th>
                <th>FIRSTNAME</th>
                <th>SURNAME</th>
                <th>MIDDLENAME</th>
                <th>REGISTRATION</th>
                <th>SEX</th>
                <th>REMARKS</th>
                </tr>
                </thead>
                <tbody>';


                    //getting starting row;

                    $startingRow = $headerRow + 2;

                    $sn = 0;
                    for ($i = $startingRow; $i <= $number_of_rows; ++$i) {
                        $rowDataArray = $sheetData[$i];
                        $remarks = "";
                        $sn++;

                        $model_s = new Students;
                        $model_s->registration_number = $rowDataArray[$columnPositions['REGISTRATION']];
                        $model_s->firstname = $rowDataArray[$columnPositions['FIRSTNAME']];
                        $model_s->middlename = $rowDataArray[$columnPositions['MIDDLENAME']];
                        $model_s->surname = strtoupper($rowDataArray[$columnPositions['SURNAME']]);
                        $model_s->sex = $rowDataArray[$columnPositions['SEX']];
                        $password = Yii::$app->getSecurity()->generatePasswordHash($model_s->surname);
                        $model_s->password = $password;
                        $model_s->is_default_password = 1;
                        $model_s->session_id = $model->session_id;

                        $transaction = Yii::$app->db->beginTransaction();
                        try {

                            $pgm_yr_id = $py_id;
                            $academic_year_id = \app\models\ProgrammeYear::findOne($pgm_yr_id)->academic_year_id;
                            $year_of_study = \app\models\ProgrammeYear::findOne($pgm_yr_id)->study_year;
                            $pintake_id = \app\models\ProgrammeYear::findOne($pgm_yr_id)->pintake_id;
                            $model_s = Students::RegisterStudent($model_s, $pintake_id, $year_of_study, $academic_year_id);
                            $objectType = 'Students';
                            $action = "Uploaded Students" . ' ' . 'Academic Year id => ' . ' ' . $academic_year_id . ' ' . 'Year Of Study => ' . ' ' . $year_of_study . ' ' . 'Programme Session id => ' . ' ' . $programme_session_id;
                            \app\models\UserAuditTrail::logAudit($action, $objectType);

                            if ($model_s->hasErrors()) {
                                $transaction->rollBack();
                                $remarks = "Not Uploaded" . ' ' . print_r($model_s->getErrors(), true);
                            } else {
                                $transaction->commit();
                                $remarks = "Uploaded";
                            }
                        } catch (Exception $exc) {
                            $model_s->addError('user_id', $exc->getTraceAsString());
                            $transaction->rollBack();
                        }



                        $uploadReport .= "<tr>
                <td>{$sn}</td>
                <td>$model_s->firstname</td>
                <td>$model->surname</td>
                <td>$model_s->middlename</td>
                <td>$model_s->registration_number</td>
                <td>$model_s->sex</td>
                <td>$remarks</td>
                </tr>";
                    }
                    $uploadReport .= ' </tbody>
                                      </table>';



                    //saveistory

                    $model_upload_history = new \app\models\StudentsUploadHistory();
                    $model_upload_history->user_id = Yii::$app->user->id;
                    $model_upload_history->py_id = $py_id;
                    $model_upload_history->ip_address = Yii::$app->getRequest()->getUserIP();
                    $model_upload_history->client_details = Yii::$app->getRequest()->getUserAgent();
                    $model_upload_history->upload_report = $uploadReport;
                    if ($model_upload_history->save(false)) {
                        $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
                        $filepath = 'uploads/students_upload_history/' . md5($model_upload_history->students_upload_history_id) . '.' . $model->file->extension;
                        $model_upload_history->uploaded_file = $filepath;
                        if ($model_upload_history->save(false)) {
                            $model->file->saveAs($filepath);
                        }
                    }
                }
            }
        }



        $searchModel = new \app\models\VwPYStudentsSearch();
        $condition = "py_id = {$py_id}";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);

        return $this->render('programme_year_students', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'py_id' => $py_id,
                    'model' => $model,
        ]);
    }

    public function actionStudentsUploadExcelTemplate($py_id) {
        if (!Yii::$app->user->can('/students/students-upload-excel-template')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        echo $this->renderAjax('_students_upload_excel_template', ['py_id' => $py_id]);
    }

    /**
     * Finds the Students model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Students the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Students::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
