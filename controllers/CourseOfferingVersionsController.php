<?php

namespace app\controllers;

use Yii;
use app\models\CourseOfferingVersions;
use app\models\CourseOfferingVersionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CourseOffering;
use app\controllers\CourseOfferingController;
use app\models\CourseOfferingSearch;

/**
 * CourseOfferingVersionsController implements the CRUD actions for CourseOfferingVersions model.
 */
class CourseOfferingVersionsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CourseOfferingVersions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseOfferingVersionsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single CourseOfferingVersions model.
     * @param integer $id
     * @return mixed
     */
 public function actionView($id, $activeTab = 'course_version-tab', $subaction = NULL, $course_id_to_delete = NULL) {
//        if (!Yii::$app->user->can('/course-offering-versions/view')) {
//            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
//        }
        
          if (Yii::$app->request->post('hasEditable') && isset($_POST['CourseOffering'])) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $message = '';
            $out = '';
            $record_id = Yii::$app->request->post('editableKey');
            $model1 = \app\models\CourseOffering::findOne($record_id);
            $oldModel = $model1;
            $post = [];
            $posted = current($_POST['CourseOffering']);
            $post['CourseOffering'] = $posted;

            if ($model1->load($post)) {
                if (!$model1->save(false)) {
                    $message = 'Error occured';
                } else {

                    $objectType = 'CourseOfferingVersions';
                    $action = "Updated Course Offering ";
                    \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model1);
                    $out = \app\models\Schools::findOne($model1->admistered_by)->school_name;
                    
                }
            } else {
                $message = 'Error occured';
            }
            return ['output' => $out, 'message' => $message];
        }
        
        if ($subaction == 'delete_course') {
            \app\models\CourseOffering::deleteAll("course_offering_id = {$course_id_to_delete}");
            $objectType = 'CourseOfferingVersions';
            $action = "Deleted course_offering_id" . ' ' . "course_offering_id =>" . $course_id_to_delete;
            \app\models\UserAuditTrail::logAudit($action, $objectType);
            $this->redirect(['view', 'id' => $id, 'activeTab' => 'course-tab']);
        }

        $model_coff = new \app\models\CourseOffering();
        $model_coff->course_offering_version_id = $id;
        if ($model_coff->load(Yii::$app->request->post())) {
           $course_offering_version_id = $id;
           $course_id =  $model_coff->course_id;
           $no_of_course_per_intake = count(\app\models\CourseOffering::find()->where(" course_offering_version_id = {$course_offering_version_id}  AND  course_id = {$course_id}  AND admistered_by = {$model_coff->admistered_by} ")->all());
           if ($no_of_course_per_intake >= 1) {
               $model_coff->addError('course_id', 'This course already exist in this School');
           }  else {
            if ($model_coff->save()) {
                $objectType = 'CourseOfferingVersions';
                $action = "Added course_offering" . ' ' . "course_offering_id =>" . $model_coff->course_offering_id . ' ' . "Course Offering version id =>" . $id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                return $this->redirect(['view', 'id' => $id, 'activeTab' => 'course-tab',]);
            }
           }
        }
     
        $model = $this->findModel($id);
        
        $condition = "course_offering_version_id = {$id}";

        $courseOfferingSearchModel = new \app\models\CourseOfferingSearch();

        $courseOfferingDataProvider = $courseOfferingSearchModel->search(\Yii::$app->request->queryParams, $condition);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $objectType = 'CourseOfferingVersions';
            $action = "Created Course Offering" . ' ' . $model->courseOfferings . ' ' . "course_offering_version_id =>" . $model->course_offering_version_id;
            \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['view', 'id' => $model->course_offering_version_id]);
        } else {
            return $this->render('view', [
                        'model' => $model,
                        'courseOfferingSearchModel' => $courseOfferingSearchModel,
                        'courseOfferingDataProvider' => $courseOfferingDataProvider,
                        'model_coff' => $model_coff,
                        'activeTab' => $activeTab,
            ]);
        }
    }

    /**
     * Creates a new CourseOfferingVersions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CourseOfferingVersions;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->course_offering_version_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CourseOfferingVersions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->course_offering_version_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CourseOfferingVersions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CourseOfferingVersions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CourseOfferingVersions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CourseOfferingVersions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
