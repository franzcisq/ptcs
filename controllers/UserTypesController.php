<?php

namespace app\controllers;

use Yii;
use app\models\UserTypes;
use app\models\UserTypesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserTypesController implements the CRUD actions for UserTypes model.
 */
class UserTypesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserTypesSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single UserTypes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->user_type_id]);
        } else {
        return $this->render('view', ['model' => $model]);
}
    }

    /**
     * Creates a new UserTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserTypes;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                 $objectType = 'UserTypes';
            $action = "Created UserTypes, user_type_id =>" . ' ' . $model->user_type_id;
            \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['view', 'id' => $model->user_type_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldModel = $model;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
               $objectType = 'UserTypes';
            $action = "Updated UserTypes ";
            \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
            return $this->redirect(['view', 'id' => $model->user_type_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
   $objectType = 'UserTypes';
        $action = "Deleted UserTypes user_type_id => " . ' ' . $id;
        \app\models\UserAuditTrail::logAudit($action, $objectType);
        return $this->redirect(['index']);
    }

    /**
     * Finds the UserTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserTypes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
