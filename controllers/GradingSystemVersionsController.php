<?php

namespace app\controllers;

use Yii;
use app\models\GradingSystemVersions;
use app\models\GradingSystemVersionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\StudentYearOfStudySearch;
use yii\filters\AccessControl;

/**
 * GradingSystemVersionsController implements the CRUD actions for GradingSystemVersions model.
 */
class GradingSystemVersionsController extends Controller {

    public function behaviors() {
        return [
              'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','delete','update','gradingsystem','student-grading-system'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GradingSystemVersions models.
     * @return mixed
     */
    public function actionIndex() {
//        
//        if (!Yii::$app->user->can('/grading-system-versions/index')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        
        $searchModel = new GradingSystemVersionsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionGradingsystem() {
//         if (!Yii::$app->user->can('/grading-system-versions/gradingsystem')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $searchModel = new GradingSystemVersionsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('gradingsystem', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionStudentGradingSystem() {
        $user_id = (\app\models\Users::findOne(\yii::$app->user->identity->id)->user_id);
        $cond = "user_id =$user_id ";
        $searchModel = new StudentYearOfStudySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $cond);
        return $this->render('_student_grading_system', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'user_id' => $user_id,
        ]);

    }
    

    
    
    
    /**
     * Displays a single GradingSystemVersions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//   if (!Yii::$app->user->can('/grading-system-versions/view')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->grading_system_version_id]);
        } else {
            return $this->render('view', ['model' => $model,
                        'model_gs' => $model_gs,
            ]);
        }
    }

    /**
     * Creates a new GradingSystemVersions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
//     if (!Yii::$app->user->can('/grading-system-versions/create')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model = new GradingSystemVersions;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                 $objectType = 'GradingSystemVersions';
           $action = "Created GradingSystemVersions id =>".' '.$model->grading_system_version_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['view', 'id' => $model->grading_system_version_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GradingSystemVersions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
//          if (!Yii::$app->user->can('/grading-system-versions/update')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model = $this->findModel($id);
         $oldModel = $model;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          $objectType = 'GradingSystemVersions';
          $action = "Updated GradingSystemVersions ";
                \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
            return $this->redirect(['view', 'id' => $model->grading_system_version_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GradingSystemVersions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
//     if (!Yii::$app->user->can('/grading-system-versions/delete')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model=  $this->findModel($id);
        $this->findModel($id)->delete();
         $objectType = 'GradingSystemVersions';
           $action = "Deleted GradingSystemVersions  id =>".' '.$model->grading_system_version_id;
             \app\models\UserAuditTrail::logAudit($action, $objectType);

        return $this->redirect(['index']);
    }

    /**
     * Finds the GradingSystemVersions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GradingSystemVersions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = GradingSystemVersions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
