<?php

namespace app\controllers;

use Yii;
use app\models\BoardMembers;
use app\models\BoardMembersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BoardMembersController implements the CRUD actions for BoardMembers model.
 */
class BoardMembersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BoardMembers models.
     * @return mixed
     */
    public function actionIndex($school_board_id)
    {
         $model = new BoardMembers;
         
        $searchModel = new BoardMembersSearch;
        $condition = " school_board_id = {$school_board_id}";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),$condition);
        
        if ($model->load(Yii::$app->request->post())) {
            $model->school_board_id = $school_board_id;
            $model->is_active = 1;
            $model->user_id = Yii::$app->user->id;
          if($model->save(false)){
        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model'=> $model,
            'school_board_id'=>$school_board_id,
        ]);
          }
            
        }

        return $this->renderAjax('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'model'=> $model,
            'school_board_id'=>$school_board_id,
        ]);
    }

    /**
     * Displays a single BoardMembers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->board_members_id]);
        } else {
        return $this->render('view', ['model' => $model]);
}
    }

    /**
     * Creates a new BoardMembers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BoardMembers;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->board_members_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BoardMembers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->board_members_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BoardMembers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BoardMembers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BoardMembers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BoardMembers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
