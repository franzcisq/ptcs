<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\PrimaryStudentYearOfStudy $model
 */

$this->title = 'Create Primary Student Year Of Study';
$this->params['breadcrumbs'][] = ['label' => 'Primary Student Year Of Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="primary-student-year-of-study-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
