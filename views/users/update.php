<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */

$this->title = 'Update Profile: ' . ' ' .Yii::$app->user->identity->firstname." ".Yii::$app->user->identity->surname." (".Yii::$app->user->identity->username.")";;
$this->params['breadcrumbs'][] = ['label' => 'Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->user->identity->firstname." ".Yii::$app->user->identity->surname." "."(".Yii::$app->user->identity->username.")", 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'Update Profile';
?>
<div class="users-update">
    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
