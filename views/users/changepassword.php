<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\builder\TabularForm;

$this->title = 'Change Your Password';
$this->params['breadcrumbs'][] = ['label' => 'My Profile', 'url' => ['profile']];
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-changepassword" style="padding-left: 250px; padding-right: 250px;">
    <p>Please fill out the following fields to change your old password...!</p>
      <?php if (Yii::$app->session->hasFlash('errorMessage')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <h5><i class="icon fa fa-check"></i>Error!</h5>
            <?= \Yii::$app->session->getFlash('errorMessage'); ?>
        </div>
    <?php endif; ?>
     <?php if (Yii::$app->session->hasFlash('erroroldpwd')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <h5><i class="icon fa fa-check"></i>Error!</h5>
            <?= \Yii::$app->session->getFlash('erroroldpwd'); ?>
        </div>
    <?php endif; ?>
    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL,]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
               'oldpass' => ['type' => Form::INPUT_WIDGET,
                'label' => 'Old Password',
                'widgetClass' => kartik\password\PasswordInput::classname(), 'options' => ['pluginOptions' => [
                        'showMeter' => false,
                        'toggleMask' => false
                    ]],
                ],
//            'oldpass' => ['type' => Form::INPUT_PASSWORD,
//                'label' => 'Old Password',
//                'options' => ['placeholder' => 'Enter Password...',]],
            'newpass' => ['type' => Form::INPUT_WIDGET,
                'label' => 'New Password',
                'widgetClass' => kartik\password\PasswordInput::classname(), 'options' => ['pluginOptions' => [
                        'showMeter' => false,
                        'toggleMask' => false
                    ]],
                ],
            'repeatnewpass' => ['type' => Form::INPUT_WIDGET,
                'label' => 'Re-Type Password',
                'widgetClass' => kartik\password\PasswordInput::classname(), 'options' => ['pluginOptions' => [
                        'showMeter' => false,
                        'toggleMask' => false
                    ]],],
        ]
    ]); 
    echo Html::submitButton( Yii::t('app', 'Change'), ['class' =>  'btn btn-primary pull-left']);
    ActiveForm::end();?>
    </div> 
   
       
