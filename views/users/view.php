<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */
$this->title = $model->firstname . ' ' . $model->middlename . ' ' . $model->surname;
$this->params['breadcrumbs'][] = ['label' => 'Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">
    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => 'view',
        'bordered' =>$bordered,
        'striped' => $striped,
        'condensed' => $condensed,
        'responsive' => $responsive,
         'hideIfEmpty'=>true,
        'hover' => true,
        'hAlign' => $hAlign,
        'vAlign' => $vAlign,
        'fadeDelay' => $fadeDelay,
        //'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' =>'&nbsp',
            'type' => DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
               [
                'group' => true,
                  'label' => 'Staff Profile'.' '."<i class='glyphicon glyphicon-user'></i>". Html::a("<i>Update</i>&nbsp<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['users/updateidentification', 'id' => $model->user_id]), ['class' => 'btn btn success pull-right']) . '<span></li>',
                'rowOptions' => ['class' => 'default']
            ],
                [
                'group' => true,
                'label' => '   <div class="row">
            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img  src="' . $model->profile_picture . '" width=800 height=900" alt="Profile Photo">
                </a>
            </div>

        </div>',
            ],
//            'user_id',
//            'student_status_id',
//            'registration_number',
            'surname',
            'firstname',
            'middlename',
//            'ps_id',
            'sex',
            'date_of_birth',
            'place_of_birth',
            'mailing_address',
            'telephone_no',
            'email_address:email',
//            'admission_date',
//            'sponsorship_type',
//            'status_comments',
            [
                'attribute' => 'completion_date',
                'format' => ['date', (isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
                'type' => DetailView::INPUT_WIDGET,
                'widgetOptions' => [
                    'class' => DateControl::classname(),
                    'type' => DateControl::FORMAT_DATE
                ]
            ],
            'username',
//            'password',
            //'is_active',
            [
                'attribute'=>'is_active',
                'label'=>'Is Active?',
                'value'=>$model->is_active = 1? Active:Inactive,
                
            ],
            
            'login_counts',
            [
                'attribute' => 'last_login',
                'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A'],
                'type' => DetailView::INPUT_WIDGET,
                'widgetOptions' => [
                    'class' => DateControl::classname(),
                    'type' => DateControl::FORMAT_DATETIME
                ]
            ],
//            'user_type',
            [
                'attribute'=>'user_type',
                'label'=>'User Type',
                'value'=>$model->user_type == 2? Staff:Student,
                
            ],
            //'staff_position_id',
            [
                'attribute'=>'staff_position_id',
                'label'=>'Staff Position',
                'value'=> \app\models\StaffPositions::findOne($model->staff_position_id)->position_name,
                
            ],
            'salutation',
              [
                'group' => true,
                'label' => 'Reset Password'.' '."<i class='glyphicon glyphicon-lock'></i>". Html::a("<i>Reset</i>&nbsp<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['users/passwordreset', 'id' => $model->user_id]), ['class' => 'btn btn success pull-right']) . '<span></li>',
                'rowOptions' => ['class' => 'default']
            ],
//            [
//                'attribute'=>'last_password_update_date',
//                'format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
//                'type'=>DetailView::INPUT_WIDGET,
//                'widgetOptions'=> [
//                    'class'=>DateControl::classname(),
//                    'type'=>DateControl::FORMAT_DATE
//                ]
//            ],
//            'auth_key',
//            'password_reset_token',
//            [
//                'attribute'=>'created_at',
//                'format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
//                'type'=>DetailView::INPUT_WIDGET,
//                'widgetOptions'=> [
//                    'class'=>DateControl::classname(),
//                    'type'=>DateControl::FORMAT_DATE
//                ]
//            ],
//            'created_by',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->user_id],
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ],
        'enableEditMode' => false,
    ])
    ?>
   
 <?php //echo Html::a('<i class="glyphicon glyphicon-log-in"></i>Roles', ['admin/assignment/view', 'id' => $model->user_id], ['class' => 'btn btn-primary', 'style' => 'float:right;margin-right:5px;']); ?>
 <?= Html::a('<i class="glyphicon glyphicon-plus"></i>Levels', ['user-levels/index', 'user_id' => $model->user_id,'user_details' => $model->surname.','.$model->firstname], ['class' => 'btn btn-success', 'style' => 'float:right;margin-right:5px']); ?>
</div>