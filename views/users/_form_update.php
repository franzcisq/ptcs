<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\builder\TabularForm;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="users-form">

    <?php
   $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL,]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' => [

//'student_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Status ID...']], 
 //'is_active'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Active...']], 
//'login_counts'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Login Counts...']], 
//'user_type'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User Type...']], 
            'firstname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Firstname...', 'maxlength' => 20]],
            'middlename' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Middlename...', 'maxlength' => 20]],
            'sex' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items' => ['M' => 'Male', 'F' => 'Female'],
                'options' => ['prompt' => '']],
            'salutation' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items'=> ['Mr.'=>'Mr.','Miss.'=>'Miss.','Ms.'=>'Ms.','Sir.'=>'Sir.','Dr.'=>'Dr.','Prof.'=>'Prof.','Rev.'=>'Rev.','Other'=>'Other'],
                'options' => ['prompt' => '' ]],
            'staff_position_id' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items' => \yii\helpers\ArrayHelper::map(\app\models\StaffPositions::find()->all(), 'staff_position_id', 'position_name'),
                'options' => ['prompt' => '']],
//'completion_date'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATE]], 
//'last_login'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 
            'username' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Username...', 'maxlength' => 200]],
//'registration_number'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Registration Number...', 'maxlength'=>255]], 
            'date_of_birth' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => kartik\date\DatePicker::classname(), 'options' => ['pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true]],],
//'admission_date'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Admission Date...', 'maxlength'=>255]], 
//'status_comments'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Status Comments...', 'maxlength'=>255]], 
            'surname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Surname...', 'maxlength' => 20]],
//'sponsorship_type'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Sponsorship Type...', 'maxlength'=>1]], 
//'place_of_birth'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Place Of Birth...', 'maxlength'=>50]], 
            'mailing_address' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Mailing Address...', 'maxlength' => 150]],
            'telephone_no' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Telephone No...', 'maxlength' => 150]],
            'email_address' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Email Address...', 'maxlength' => 100]],
//'student_id'=>['type'=> TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'Enter ...']],
            // 'password' => ['type' => Form::INPUT_PASSWORD, 'options' => ['placeholder' => 'Enter Password...', 'maxlength' => 255]],
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>
