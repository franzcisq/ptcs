<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\AuthAssignmentSearch $searchModel
 */
$this->title = 'User Access Levels';
$this->params['breadcrumbs'][] = ['label' => $user_details, 'url' => ['/staff-manager/view', 'id' => $user_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assignment-index">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'assignment_id',
//            [
//                'label' => 'Level name',
//                'attribute' => 'institution_structure_id',
//                'value' => function ($model) {
//                    return ($model->institutionStructure->institution_name);
//                },
//            ],
            [
                'label' => 'Role',
                'attribute' => 'item_name',
                'value' => function ($model) {
                    return ($model->itemName->description);
                },
            ],
//            'user_id',
            'created_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{delete}',
                'urlCreator' => function ($action, $model) use ($user_details) {
                    if ($action === 'delete') {
                        $url = Url::to(['delete', 'id' => $model->assignment_id, 'user_id' => $model->user_id]);
                        return $url;
                    }
                }
                    ],
                ],
                'responsive' => true,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => false,
//        'panel' => [
//            'heading'=>'<h3 class="panel-title"> </h3>',
//            'type'=>'default',
//            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index', 'user_id' => $user_id, 'user_details' => $user_details], ['class' => 'btn btn-info']),
//            'showFooter'=>false
//        ],
            ]);
            Pjax::end();

            echo $this->render('_form', ['assignmentModel' => $assignmentModel, 'user_id' => $user_id, 'user_details' => $user_details]);
            ?>

</div>
