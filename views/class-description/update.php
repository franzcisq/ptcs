<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ClassDescription $model
 */

$this->title = 'Update Class: ' . ' ' . $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->description, 'url' => ['view', 'id' => $model->class_description_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="class-description-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
