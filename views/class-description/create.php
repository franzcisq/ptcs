<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ClassDescription $model
 */

$this->title = 'Add Class ';
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="class-description-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
