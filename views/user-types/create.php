<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UserTypes $model
 */

$this->title = 'Add User Type';
$this->params['breadcrumbs'][] = ['label' => 'User Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-types-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
