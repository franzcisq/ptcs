<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\OlevelStudentYearOfStudySearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="olevel-student-year-of-study-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'olevel_student_year_of_study_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'fy_id') ?>

    <?= $form->field($model, 'student_year_status_id') ?>

    <?= $form->field($model, 'year_of_study') ?>

    <?php // echo $form->field($model, 'results') ?>

    <?php // echo $form->field($model, 'year_units') ?>

    <?php // echo $form->field($model, 'year_grade_points') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
