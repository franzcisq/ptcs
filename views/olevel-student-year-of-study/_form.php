<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\OlevelStudentYearOfStudy $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="olevel-student-year-of-study-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...', 'maxlength'=>20]], 

'fy_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fy ID...']], 

'student_year_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Year Status ID...']], 

'year_of_study'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Of Study...']], 

'results'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Results...']], 

'year_units'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Units...']], 

'year_grade_points'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Grade Points...']], 

'status'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Status...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
