<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Wards $model
 */

$this->title = 'Create Wards';
$this->params['breadcrumbs'][] = ['label' => 'Wards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wards-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
