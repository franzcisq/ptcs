<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\CourseOfferingSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="course-offering-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'course_offering_id') ?>

    <?= $form->field($model, 'admistered_by') ?>

    <?= $form->field($model, 'course_id') ?>

    <?= $form->field($model, 'course_offering_version_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
