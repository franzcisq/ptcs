<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\CourseOffering $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="course-offering-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'admistered_by'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Admistered By...', 'maxlength'=>11]], 

'course_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Course ID...', 'maxlength'=>10]], 

'course_offering_version_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Course Offering Version ID...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
