<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\VwOlevelStudents $model
 */

$this->title = 'Update Vw Olevel Students: ' . ' ' . $model->user_id;
$this->params['breadcrumbs'][] = ['label' => 'Vw Olevel Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_id, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vw-olevel-students-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
