<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Inbox $model
 */

$this->title = 'Create Inbox';
$this->params['breadcrumbs'][] = ['label' => 'Inboxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inbox-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
