<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\InboxSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="inbox-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'inbox_id') ?>

    <?= $form->field($model, 'message_id') ?>

    <?= $form->field($model, 'sent_by') ?>

    <?= $form->field($model, 'received_by') ?>

    <?= $form->field($model, 'school_id') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
