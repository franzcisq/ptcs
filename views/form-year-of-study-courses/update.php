<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearOfStudyCourses $model
 */

$this->title = 'Update Form Year Of Study Courses: ' . ' ' . $model->fyos_course_id;
$this->params['breadcrumbs'][] = ['label' => 'Form Year Of Study Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fyos_course_id, 'url' => ['view', 'id' => $model->fyos_course_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="form-year-of-study-courses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
