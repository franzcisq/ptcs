<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearOfStudyCourses $model
 */

$this->title = 'Create Form Year Of Study Courses';
$this->params['breadcrumbs'][] = ['label' => 'Form Year Of Study Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-year-of-study-courses-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
