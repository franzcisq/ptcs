<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="form-year-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fy_id') ?>

    <?= $form->field($model, 'academic_year_id') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'year_number') ?>

    <?= $form->field($model, 'grading_system_version_id') ?>

    <?php // echo $form->field($model, 'study_year') ?>

    <?php // echo $form->field($model, 'required_year_units') ?>

    <?php // echo $form->field($model, 'form_year_status_id') ?>

    <?php // echo $form->field($model, 'approval_status_id') ?>

    <?php // echo $form->field($model, 'publish_status_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
