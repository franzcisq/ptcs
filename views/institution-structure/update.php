<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionStructure $model
 */

$this->title = 'Update Institution Structure: ' . ' ' . $model->institution_name;
$this->params['breadcrumbs'][] = ['label' => 'Institution Structure', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->institution_structure_id, 'url' => ['view', 'id' => $model->institution_structure_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="institution-structure-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
