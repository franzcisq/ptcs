<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\InstitutionStructureSearch $searchModel
 */
$model_parent = app\models\InstitutionStructure::find()->where("parent_institution_structure_id = 0")->one();

$this->title = 'Institution Structure - ' . $model_parent->institution_name;
$this->params['breadcrumbs'][] = 'Institution Structure';

function buildStructure($parent_id, $margin = 30) {
    $records = app\models\InstitutionStructure::find()->where("parent_institution_structure_id = {$parent_id} ")->all();
    $hierachy_string = "";
    foreach ($records as $key => $value) {
        $hierachy_string .= '<li class="list-group-item"><span style="margin-left:' . $margin . 'px">' . $value->institution_name . ' - ' . $value->institution_acronym . ' [' . \app\models\InstitutionType::findOne($value->institution_type_id)->institution_type_name . '] ' . Html::a("<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['institution-structure/update', 'id' => $value->institution_structure_id]), ['class' => 'pull-right']) . '<span>' . Html::a(" <i class='glyphicon glyphicon-eye-open'></i>", \yii\helpers\Url::to(['institution-structure/view', 'id' => $value->institution_structure_id]), ['class' => 'pull-right']) . '<span></li>';
        if (count(app\models\InstitutionStructure::find()->where("parent_institution_structure_id = {$value->institution_structure_id} ")->all()) > 0) {
            $margin_incre = $margin + 30;
            $hierachy_string .= buildStructure($value->institution_structure_id, $margin_incre);
        }
    }
    return $hierachy_string;
}
?>
<div class="institution-structure-index">
    <ul class="list-group">
        <li class="list-group-item"><?php echo $model_parent->institution_name . ' - ' . $model_parent->institution_acronym . ' [' . \app\models\InstitutionType::findOne($model_parent->institution_type_id)->institution_type_name . '] ' . Html::a("<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['institution-structure/update', 'id' => $model_parent->institution_structure_id]), ['class' => 'pull-right']) . '<span>' . Html::a(" <i class='glyphicon glyphicon-eye-open'></i>", \yii\helpers\Url::to(['institution-structure/view', 'id' => $model_parent->institution_structure_id]), ['class' => 'pull-right']) . '<span></li>'; ?>
<?= buildStructure($model_parent->institution_structure_id) ?>
            <br>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']) ?>
    </ul>

            <?php
//    echo GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            //'institution_structure_id',
//            'institution_name',
//            'institution_acronym',
//            'parent_institution_structure_id',
//            'institution_type',
//
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['institution-structure/view','id' => $model->institution_structure_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
//        ],
//        'responsive'=>true,
//        'hover'=>true,
//        'condensed'=>true,
//        'floatHeader'=>true,
//
//
//
//
//        'panel' => [
//            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
//            'type'=>'default',
//            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
//            'showFooter'=>false
//        ],
//    ]);
            ?>


</div>
