<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StudentFormCourses $model
 */

$this->title = $model->sfc_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Form Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-form-courses-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'sfc_id',
            'fy_id',
            'fyos_course_id',
            'score_type',
            'result',
            'grade_scored',
            'student_form_course_status_id',
            'remarks',
            'grade_points',
            'changes_log:ntext',
            'last_updated',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->sfc_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
