<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\StudentFormCoursesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-form-courses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sfc_id') ?>

    <?= $form->field($model, 'fy_id') ?>

    <?= $form->field($model, 'fyos_course_id') ?>

    <?= $form->field($model, 'score_type') ?>

    <?= $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'grade_scored') ?>

    <?php // echo $form->field($model, 'student_form_course_status_id') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'grade_points') ?>

    <?php // echo $form->field($model, 'changes_log') ?>

    <?php // echo $form->field($model, 'last_updated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
