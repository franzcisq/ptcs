<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentFormCourses $model
 */

$this->title = 'Update Student Form Courses: ' . ' ' . $model->sfc_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Form Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sfc_id, 'url' => ['view', 'id' => $model->sfc_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-form-courses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
