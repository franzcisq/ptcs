<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LoginAttempts $model
 */

$this->title = 'Update Login Attempts: ' . ' ' . $model->login_attempts_id;
$this->params['breadcrumbs'][] = ['label' => 'Login Attempts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->login_attempts_id, 'url' => ['view', 'id' => $model->login_attempts_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="login-attempts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
