<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\LoginAttemptsSearch $searchModel
 */

$this->title = 'Login Attempts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-attempts-index">
    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'login_attempts_id',
            //'user_id',
            [
              'attribute'=>'user_id',
                'label'=>'User',
                'value'=>  function ($model){
                    return app\models\Users::findOne($model->user_id)->firstname.' '.app\models\Users::findOne($model->user_id)->middlename.' '.app\models\Users::findOne($model->user_id)->surname;
                }
                
            ],
            'ip_address',
            //'successfully_attempt',
                                 [
                         'label' => 'successfully_attempt',
                        'attribute' => 'successfully_attempt',
                        'vAlign' => 'middle',
                        'width' => '120px',
                        'value' => function($model) {

                            return $model->successfully_attempt == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                        },
    
                        'format' => 'raw'
                    ],
            ['attribute'=>'last_login','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{view}',
                'buttons' => [
                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['login-attempts/view','id' => $model->login_attempts_id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i></h3>',
            'type'=>'default',
            //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
