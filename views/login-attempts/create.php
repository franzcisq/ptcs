<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\LoginAttempts $model
 */

$this->title = 'Create Login Attempts';
$this->params['breadcrumbs'][] = ['label' => 'Login Attempts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-attempts-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
