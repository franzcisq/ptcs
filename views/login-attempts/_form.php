<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\LoginAttempts $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="login-attempts-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'login_attempts_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Login Attempts ID...']], 

'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...']], 

'ip_address'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Ip Address...', 'maxlength'=>50]], 

'successfully_attempt'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Attempt...']], 

'last_login'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
