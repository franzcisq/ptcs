<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Donors $model
 */

$this->title = 'Update Donor: ' . ' ' . $model->donor_name;
$this->params['breadcrumbs'][] = ['label' => 'Donors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->donor_name, 'url' => ['view', 'id' => $model->donor_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="donors-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
