<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Donors $model
 */

$this->title = 'Add Donor';
$this->params['breadcrumbs'][] = ['label' => 'Donors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donors-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
