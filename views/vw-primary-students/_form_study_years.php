<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var app\models\StudentYearOfStudy $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-year-of-study-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [


            'academic_year' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items' => yii\helpers\ArrayHelper::map(app\models\AcademicYears::find()->all(), 'academic_year_id', 'academic_year'),
                //'items' => yii\helpers\ArrayHelper::map(app\models\StudentYearOfStudy::
                'options' => ['prompt' => 'Select']],
//            
//            'year_of_study' => ['type' => Form::INPUT_DROPDOWN_LIST,
//                'items' => \yii\helpers\ArrayHelper::map(\app\models\StudyYearClassification::find()->all(),'study_year_number','description'),
//                'options' => ['prompt' => '']],
            'year_of_study' => [
                'type' => Form::INPUT_WIDGET,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'label' => 'Study Year',
                'widgetClass' => DepDrop::className(),
                'options' => [
                    //'data' => [$model->year_of_study => \app\models\ClassDescription::find()->where(" class_number = {$model->year_of_study}")->one()->description],
                    'pluginOptions' => [
                        'depends' => ['primarystudentyearofstudy-academic_year'],
                        'placeholder' => 'Select',
                        'url' => Url::to(['/vw-primary-students/study-years']),
                    ],
                ],
                'columnOptions' => ['id' => 'year_of_study'],
            ],
//'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...', 'maxlength'=>11]], 
//'py_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Py ID...']], 
//            'student_year_status_id' => ['type' => Form::INPUT_DROPDOWN_LIST,
//                'items' => yii\helpers\ArrayHelper::map(\app\models\StudentYearStatus::find()->all(), 'student_year_status_id', 'status_desc'),
//                'options' => ['prompt' => '']],
//'gpa_obtained'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Gpa Obtained...']], 
//'year_units'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Units...']], 
//'year_grade_points'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Grade Points...']], 
//'status_comments'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Status Comments...', 'maxlength'=>255]], 
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add Study Year') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>
