<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\VwPrimaryStudents $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="vw-primary-students-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...', 'maxlength'=>11]], 

'student_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Status ID...']], 

'school_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School ID...']], 

'is_active'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Active...']], 

'login_counts'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Login Counts...']], 

'user_type'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User Type...']], 

'staff_position_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Staff Position ID...']], 

'primary_student_year_of_study_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Primary Student Year Of Study ID...', 'maxlength'=>20]], 

'cy_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Cy ID...']], 

'student_year_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Year Status ID...']], 

'year_of_study'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Of Study...']], 

'year_units'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Units...']], 

'status'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Status...']], 

'registration_number'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Registration Number...', 'maxlength'=>255]], 

'admission_date'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATE]], 

'status_comments'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Status Comments...', 'maxlength'=>255]], 

'completion_date'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATE]], 

'username'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Username...', 'maxlength'=>200]], 

'last_login'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 

'salutation'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Salutation...', 'maxlength'=>20]], 

'results'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Results...']], 

'year_grade_points'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Grade Points...']], 

'profile_picture'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Profile Picture...', 'maxlength'=>200]], 

'date_of_birth'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Date Of Birth...', 'maxlength'=>255]], 

'password'=>['type'=> Form::INPUT_PASSWORD, 'options'=>['placeholder'=>'Enter Password...', 'maxlength'=>255]], 

'surname'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Surname...', 'maxlength'=>20]], 

'firstname'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Firstname...', 'maxlength'=>20]], 

'middlename'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Middlename...', 'maxlength'=>20]], 

'sex'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Sex...', 'maxlength'=>1]], 

'place_of_birth'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Place Of Birth...', 'maxlength'=>50]], 

'mailing_address'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Mailing Address...', 'maxlength'=>150]], 

'telephone_no'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Telephone No...', 'maxlength'=>150]], 

'email_address'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Email Address...', 'maxlength'=>100]], 

'fullname'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fullname...', 'maxlength'=>62]], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
