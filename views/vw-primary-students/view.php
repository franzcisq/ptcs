<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use kartik\tabs\TabsX;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var app\models\VwPrimaryStudents $model
 */
$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Primary Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-primary-students-view">


    <?php
    ob_start();

    if ($model->profile_picture == NULL) {
        $picture = 'default-no-image/default-no-profile-image.png';
    } else {
        $picture = $model->profile_picture;
    }
    ?>
    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'hideIfEmpty' => true,
        //'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => "&nbsp",
            'type' => DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            [
                'label' => 'Photo',
                'attribute' => 'profile_picture',
                'value' => $picture,
                'format' => ['image', ['width' => '100', 'height' => '100']],
            ],
            //'user_id',
            'registration_number',
            //'surname',
            //'firstname',
            //'middlename',
            //'fullname',
            // 'sex',
            [
                'label' => 'Gender',
                'attribute' => 'sex',
                'value' => $model->sex == 'M' ? 'Male' : 'Female',
            ],
            'date_of_birth',
            // 'place_of_birth',
            'mailing_address',
            'telephone_no',
            'email_address:email',
            'admission_date',
            'date_of_birth',
            'place_of_birth',
            'mailing_address',
            'telephone_no',
            'email_address:email',
            'admission_date',
            'username',
            // 'password',
            //'is_active',
            [
                'label' => 'Is Active?',
                'attribute' => 'is_active',
                'value' => $model->is_active == 1 ? 'Active' : 'Inactive',
            ],
            //  'login_counts',
            [
                'attribute' => 'last_login',
                'value' => $model->last_login == '0000-00-00 00:00:00' ? 'Never' : $model->last_login,
            ],
            'login_counts',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->user_id],
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ],
        'enableEditMode' => false,
    ])
    ?>

    <?php
    $student = ob_get_contents();
    ob_end_clean();
    ?>

</div>

<div>
<?php ob_start(); ?>
<?php
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $model_study_yearDataProvider,
    //'filterModel' => $model_study_yearSearch,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'value' => function ($model, $key, $index, $column) {

                return GridView::ROW_COLLAPSED;
            },
            'allowBatchToggle' => true,
            'detail' => function ($model) use ($user_id) {

                return $this->render('_student_class_courses', ['user_id'=>$user_id, 'cy_id'=>$model->cy_id]);
            },
                    'detailOptions' => [
                        'class' => 'kv-state-enable',
                    ],
                    //'detailUrl' => Url::to(['student-semesters/index']),
                    'detailRowCssClass' => GridView::TYPE_DEFAULT,
                    'pageSummary' => false,
                ],
                         [
                        'attribute' => 'year_of_study',
                        'value' => function ($model) {
                            return app\models\ClassDescription::find()->where("class_number = {$model->year_of_study}")->one()->description;
                            }
                        
                        ],
                    [
                        'label' => 'Academic Year',
                        'attribute' => 'cy_id',
                        'value' => function ($model) {
                            return app\models\AcademicYears::findOne(\app\models\ClassYear::findOne($model->cy_id)->academic_year_id)->academic_year;
                        }
                    ], 
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => "{delete}",
                    'buttons' => [
//                    'update' => function ($url, $model) {
//                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['student-year-of-study/view', 'id' => $model->student_year_of_study_id, 'edit' => 't']), [
//                                    'title' => Yii::t('yii', 'Edit'),
//                        ]);
//                    }
                    ],
                    'urlCreator' => function ($action, $model) {

                if ($action == 'delete') {
                    return Url::to(['vw-primary-students/view',
                                'academic_year_to_delete' => $model->primary_student_year_of_study_id,
                                'subaction' => 'delete_student_year_of_study',
                                'id' => $model->user_id,
                    ]);
                }
            }
                ],
            ],
            'responsive' => true,
            'hover' => true,
            'condensed' => true,
            'floatHeader' => false,
//                'panel' => [
//                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>  </h3>',
//                    'type' => 'default',
//                    'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']), 'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
//                    'showFooter' => false
//                ],
        ]);
        Pjax::end();
        ?>
            <?= Yii::$app->session->getFlash('error'); ?>
            <?= 
            $this->render('_form_study_years', [
                'model' => $student_yearModel,
                'user_id' => $user_id,
            ]);
        
            ?>

            <?php
            $study_year = ob_get_contents();
            ob_end_clean();
            ?>



        </div>


            <?php
            echo TabsX::widget([
                'items' => [
                    [
                        'label' => 'Student Details',
                        'content' => $student,
                        //'active' => true,
                        'options' => ['id' => 'student-tab'],
                        'active' => ($activeTab == 'student-tab'),
                    ],
                    [
                        'label' => 'Study Years',
                        'content' => $study_year,
                        //'active' => false,
                        'options' => ['id' => 'Years-tab'],
                        'active' => ($activeTab == 'Years-tab'),
                    ],
                ],
                'bordered' => true,
            ]);
            ?>
