<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentForms $model
 */

$this->title = 'Update Student Forms: ' . ' ' . $model->sf_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sf_id, 'url' => ['view', 'id' => $model->sf_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-forms-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
