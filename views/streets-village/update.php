<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StreetsVillage $model
 */

$this->title = 'Update Streets Village: ' . ' ' . $model->street_id;
$this->params['breadcrumbs'][] = ['label' => 'Streets Villages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->street_id, 'url' => ['view', 'id' => $model->street_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="streets-village-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
