<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\UserAuditTrailSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-audit-trail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'user_audit_trail_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'ip_address') ?>

    <?= $form->field($model, 'details') ?>

    <?= $form->field($model, 'datecreated') ?>
    
     <?= $form->field($model, 'client_details') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
