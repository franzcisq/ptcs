<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\UserAuditTrail $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-audit-trail-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 3,
    'attributes' => [

'user_audit_trail_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User Audit Trail ID...']], 

'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...', 'maxlength'=>11]], 

'datecreated'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 

'ip_address'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Ip Address...', 'maxlength'=>50]], 

'details'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Details...', 'maxlength'=>255]], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
