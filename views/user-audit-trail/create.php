<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UserAuditTrail $model
 */

$this->title = 'Create User Audit Trail';
$this->params['breadcrumbs'][] = ['label' => 'User Audit Trails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-audit-trail-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
