<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolSponsors $model
 */

$this->title = \app\models\Sponsors::findOne($model->school_sponsor_id)->sponsor_name;
$this->params['breadcrumbs'][] = ['label' => 'School Sponsors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-sponsors-view">
    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>'&nbsp',
            'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            //'school_sponsor_id',
//            'school_id',
            [
                'label'=>'School',
                'attribute'=>'school_id',
                'value'=>  \app\models\Schools::findOne($model->school_id)->school_name,
                
            ],
//            'sponsor_id',
            [
                'label'=>'Sponsor',
                'attribute'=>'sponsor_id',
                'value'=> \app\models\Sponsors::findOne($model->sponsor_id)->sponsor_name,
                
            ],
            'what_sponsored:ntext',
//            'amount',
              [

                            'label' => 'Amount Sponsored',
                            'attribute' => 'amount',
                            'hAlign' => 'right',
                            'value' => $model->amount,
                            'format' => 'raw',
                            'format' => ['decimal', 2]
                        ],
            [
                'attribute'=>'date_created',
                'format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A'],
                'type'=>DetailView::INPUT_WIDGET,
                'widgetOptions'=> [
                    'class'=>DateControl::classname(),
                    'type'=>DateControl::FORMAT_DATETIME
                ]
            ],
            [
                'attribute'=>'date_sponsored',
                'format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
                'type'=>DetailView::INPUT_WIDGET,
                'widgetOptions'=> [
                    'class'=>DateControl::classname(),
                    'type'=>DateControl::FORMAT_DATE
                ]
            ],
            'is_visible',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->school_sponsor_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>false,
    ]) ?>

</div>
