<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolSponsors $model
 */

$this->title = 'Update School Sponsors: ' . ' ' . $model->school_sponsor_id;
$this->params['breadcrumbs'][] = ['label' => 'School Sponsors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->school_sponsor_id, 'url' => ['view', 'id' => $model->school_sponsor_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="school-sponsors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
