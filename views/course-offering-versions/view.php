<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use kartik\tabs\TabsX;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\CourseOffering;
use kartik\grid\EditableColumn;
use yii\helpers\ArrayHelper;
use kartik\editable\Editable;

/**
 * @var yii\web\View $this
 * @var app\models\CourseOfferingVersions $model
 */
$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Course Offering Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-offering-versions-view">

    <?php
    ob_start();
    ?>
    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'buttons2' => '{view} {reset} {save}',
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => "&nbsp",
            'type' => DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            // 'course_offering_version_id',
            'description',
           [
               'label'=>'School Type',
               'attribute' => 'school_type_id',
               'value'=> \app\models\SchoolType::findOne($model->school_type_id)->school_type,
           ],
           
            [
                'attribute' => 'status',
                'label' => 'Status',
                'format' => 'raw',
                'value' => $model->status == 1 ? 'Active' : 'Inactive'
            ],
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->course_offering_version_id],
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ],
        'enableEditMode' => true,
    ])
    ?>

    <?php
    $contents = ob_get_contents();
    ob_end_clean();
    ?>

    <?php
    ob_start();
    ?>

    <?php
    $gridColumns = [



        [ 'label' => 'Course',
            'attribute' => 'course_id',
            'value' => function($model) {
                return \app\models\Courses::findOne($model->course_id)->course_code . '- ' . \app\models\Courses::findOne($model->course_id)->course_title;
            },
            'width' => '400px',
        ],

        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'admistered_by',
            'label' => 'Administered By',
            'value' => function($model) {
                $modelSchool = \app\models\Schools::findOne($model->admistered_by);
               return isset($modelSchool->school_name) ? $modelSchool->school_name . " [" . \app\models\SchoolType::findOne($modelSchool->school_type_id)->school_type . "]" : "Click to Select School";
            },
            'editableOptions' => [
                'header' => 'administered By',
                'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'data'=>\yii\helpers\ArrayHelper::map(app\models\Schools::find()->where(" school_type_id = {$model->school_type_id}")->all(),'school_id','school_name'),
               
            ],
            'width' => '400px',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'urlCreator' => function($action, $model) {
                if ($action == 'delete') {
                    return \yii\helpers\Url::to(['course-offering-versions/view', 'course_id_to_delete' => $model->course_offering_id, 'subaction' => 'delete_course', 'id' => $model->course_offering_version_id]);
                }
            }
                ],
            ];

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $courseOfferingDataProvider,
                'filterModel' => $courseOfferingSearchModel,
                'columns' => $gridColumns,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                ],
                'id' => 'targtet-types-grid-view',
            ]);
            ?>

            <div class="course-offering-create">  
            <?=
            $this->render('_form_course_offering', [
                'model' => $model_coff,
                'school_type_id'=>$model->school_type_id,
            ])
            ?>
            </div>

                <?php
                $course_offerings = ob_get_contents();
                ob_end_clean();
                ?>
            <?php
            echo TabsX::widget([
                'items' => [
                    [
                        'label' => 'Course Offering Version',
                        'content' => $contents,
                        //'active' => true,
                        'options' => ['id' => 'course_version-tab'],
                        'active' => ($activeTab == 'course_version-tab'),
                    ],
                    [
                        'label' => 'Courses',
                        'content' => $course_offerings,
                        //'active' => false,
                        'options' => ['id' => 'course-tab'],
                        'active' => ($activeTab == 'course-tab'),
                    ],
                ],
                'bordered' => true,
            ]);
            ?>

</div>

