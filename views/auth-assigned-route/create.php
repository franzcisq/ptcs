<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\AuthAssignedRoute $model
 */

$this->title = 'Create Auth Assigned Route';
$this->params['breadcrumbs'][] = ['label' => 'Auth Assigned Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assigned-route-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
