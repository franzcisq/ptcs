<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\AuthAssignedRoute $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="auth-assigned-route-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'auth_item_name'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Auth Item Name...', 'maxlength'=>255]], 

'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...']], 

'institution_structure_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Institution Structure ID...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
