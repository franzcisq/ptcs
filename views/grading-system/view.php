<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystem $model
 */

$this->title = $model->grading_system_id;
$this->params['breadcrumbs'][] = ['label' => 'Grading Systems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grading-system-view">
 
    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'grading_system_id',
            'grade',
            'min_score',
            'max_score',
            'grade_points_formula',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->grading_system_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>


