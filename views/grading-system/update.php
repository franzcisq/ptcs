<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystem $model
 */

$this->title = 'Update Grading System: ' . ' ' . $model->grading_system_id;
$this->params['breadcrumbs'][] = ['label' => 'Grading Systems', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->grading_system_id, 'url' => ['view', 'id' => $model->grading_system_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="grading-system-update">

  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
