<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\GradingSystemSearch $searchModel
 */
$this->params['breadcrumbs'][] = ['label'=>'Grading Score System','url'=>['/grading-system-versions/index']];
$this->title = ' ' . \app\models\GradingSystemVersions::findOne($grading_system_version_id)->description;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grading-system-index">

    <?php
    $gridColumns = [
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'grade',
            'label' => 'Grade',
            'editableOptions' => [
                'header' => 'Grade',
                'inputType' => \kartik\editable\Editable::INPUT_TEXT,
            ],
            'width' => '100px',
            'hAlign' => 'left',
        ],
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'order',
            'editableOptions' => [
                'header' => 'Order',
                'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                'options' => [
                    'pluginOptions' => ['min' => 1, 'max' => 10]
                ]
            ],
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'width' => '7%',
            'format' => ['decimal', 0],
            'pageSummary' => true,
        ],
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'min_score',
            'editableOptions' => [
                'header' => 'Minimum Score',
                'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                'options' => [
                    'pluginOptions' => ['min' => 0, 'max' => 100]
                ]
            ],
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'width' => '7%',
            'format' => ['decimal', 0],
            'pageSummary' => true,
        ],
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'max_score',
            'editableOptions' => [
                'header' => 'Maximum Score',
                'inputType' => \kartik\editable\Editable::INPUT_SPIN,
                'options' => [
                    'pluginOptions' => ['min' => 0, 'max' => 100]
                ]
            ],
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'width' => '7%',
            'format' => ['decimal', 0],
            'pageSummary' => true,
        ],
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'grade_points_formula',
            'label' => 'Grade Points Formula',
            'editableOptions' => [
                'header' => 'Grade Points Formula',
                'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                'inputType' => \kartik\editable\Editable::INPUT_TEXT,
            ],
            'width' => '50px',
            'hAlign' => 'right',
            'vAlign' => 'left',
        ],
           [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'definition',
            'label' => 'Remarks',
            'editableOptions' => [
                'header' => 'Remarks',
                'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                'inputType' => \kartik\editable\Editable::INPUT_TEXT,
            ],
            'width' => '100px',
            'hAlign' => 'right',
            'vAlign' => 'left',
        ],
        
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['grading-system/view', 'id' => $model->grading_system_id, 'edit' => 't']), [
                                'title' => Yii::t('yii', 'Edit'),
                    ]);
                }
                    ],
                    'options' => [
                        'style' => 'width:20px;'
                    ],
                ],
            ];

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'pjax' => true,
                'pjaxSettings' => [
                    'neverTimeout' => true,
                ],
                'id' => 'targtet-types-grid-view',
            ]);
            ?>

            <?=
            $this->render('_form', [
                'model' => $model_gs,
            ])
            ?>

</div>
