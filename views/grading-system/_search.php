<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystemSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="grading-system-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'grading_system_id') ?>

    <?= $form->field($model, 'grade') ?>

    <?= $form->field($model, 'min_score') ?>

    <?= $form->field($model, 'max_score') ?>

    <?= $form->field($model, 'grade_points_formula') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
