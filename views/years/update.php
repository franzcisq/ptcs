<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Years $model
 */

$this->title = 'Update Years: ' . ' ' . $model->year_id;
$this->params['breadcrumbs'][] = ['label' => 'Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->year_id, 'url' => ['view', 'id' => $model->year_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="years-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
