<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClassesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-classes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sc_id') ?>

    <?= $form->field($model, 'student_year_of_study_id') ?>

    <?= $form->field($model, 'cyos_id') ?>

    <?= $form->field($model, 'cyos_status_id') ?>

    <?= $form->field($model, 'marks_obtained') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
