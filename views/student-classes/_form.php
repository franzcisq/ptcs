<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClasses $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-classes-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'student_year_of_study_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Year Of Study ID...']], 

'cyos_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Cyos ID...']], 

'cyos_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Cyos Status ID...']], 

'marks_obtained'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Marks Obtained...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
