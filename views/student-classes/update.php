<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClasses $model
 */

$this->title = 'Update Student Classes: ' . ' ' . $model->sc_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sc_id, 'url' => ['view', 'id' => $model->sc_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-classes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
