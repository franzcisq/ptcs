<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClasses $model
 */

$this->title = 'Create Student Classes';
$this->params['breadcrumbs'][] = ['label' => 'Student Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-classes-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
