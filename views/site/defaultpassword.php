<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\builder\TabularForm;

$name = Yii::$app->user->identity->firstname . " " . Yii::$app->user->identity->surname . " (" . Yii::$app->user->identity->username . ")";
$this->title = 'Change Your Default Password';
$this->title = 'Hi,' . $name . '  ' . '.' . ' Please change your default Password to continue...!';
?>


<div style="padding-right: 250px; padding-left: 250px;">
    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL,]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'newpass' => ['type' => Form::INPUT_WIDGET,
                'label' => 'New Password',
                'widgetClass' => kartik\password\PasswordInput::classname(), 'options' => ['pluginOptions' => [
                        'showMeter' => false,
                        'toggleMask' => false
                    ]],
            ],
            'repeatnewpass' => ['type' => Form::INPUT_WIDGET,
                'label' => 'Re-Type Password',
                'widgetClass' => kartik\password\PasswordInput::classname(), 'options' => ['pluginOptions' => [
                        'showMeter' => false,
                        'toggleMask' => false
                    ]],],
        ]
    ]);
    
//    echo Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn btn-primary pull-left']);
//    ActiveForm::end(); ?>
      <?php if (Yii::$app->session->hasFlash('errorMessage')): ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
                <h5><i class="icon fa fa-check"></i>Error!</h5>
                <?= \Yii::$app->session->getFlash('errorMessage'); ?>
            </div>
        <?php endif;
         echo Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn btn-primary pull-left']);
    ActiveForm::end(); ?>
        
        
     
</div>



