<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

//$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
$model_ins = \app\models\InstitutionStructure::find()->where("parent_institution_structure_id = 0")->one();
//$this->title = $model_ins->institution_name . " (" . $model_ins->institution_acronym . ")";
$this->title = 'Password Recovery';
?>
        
         <div class="col-sm-3"><center><img src="img/logo.png" alt="Responsive image" width="100" height="100" class="img-responsive" align="middle"/></center><br>
<small><center>
       <?php
                echo $model_ins->institution_name;
                echo "&nbsp;(";
                echo $model_ins->institution_acronym;
                echo ")&nbsp;";
       ?>
    </center></small></div>
<p><em>Confirmation will be sent to your E-mail Address!. Please provide a valid :</em></p>
<div style="padding-left: 350px; padding-right: 350px;">

    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                //'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
    ]);
    ?>
    <?=
    $form->field($model, 'E_mail')->widget(\yii\widgets\MaskedInput::className(), [
        'name' => 'E_mail',
        'clientOptions' => [
            'alias' => 'email'
        ],
    ])
    ?>

    <?=
    $form->field($model, 'Phone_number')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+255999999999',
        'name' => 'phone',
    ])
    ?>
    <?=
    $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
        // configure additional widget properties here
        'template' => '{image}{input}',
    ])
    ?>

    <h5>New verification code ?  Click on the code above!</h5>



    <?php
//$form->field($model, 'rememberMe', [
//    'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//])->checkbox();
    ?>
<?php if (Yii::$app->session->hasFlash('failMessage')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <h5><i class="icon fa fa-check"></i>Failed!</h5>
        <?= \Yii::$app->session->getFlash('failMessage'); ?>
        </div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash('errorMessage')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button"></button>
            <h5><i class="icon fa fa-check"></i>Failed!</h5>
        <?= \Yii::$app->session->getFlash('errorMessage'); ?>
        </div>
<?php endif; ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
<?= Html::submitButton('Send', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>


</div>

</div>
<hr>
