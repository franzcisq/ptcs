<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */
$user_model = app\models\Users::findOne($id);
$this->title = 'Welcome: ' . ' ' .$user_model->firstname." ".$user_model->surname." (".$user_model->username.")".' '.'Reset Your Aris Password!';
?>
<div class="users-update">
    <?= $this->render('_passwordreset_form', [
        'model' => $model,
    ]) ?>

</div>
