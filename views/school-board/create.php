<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolBoard $model
 */

$this->title = 'Create School Board';
$this->params['breadcrumbs'][] = ['label' => 'School Boards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-board-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
