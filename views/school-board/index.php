<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\SchoolBoardSearch $searchModel
 */
$this->title = 'School Boards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-board-index">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'school_board_id',
//            'school_id',
            [
                'label' => 'School',
                'attribute' => 'school_id',
                'value' => function ($model) {
                    return \app\models\Schools::findOne($model->school_id)->school_name;
                }
            ],
            'name',
            'description:ntext',
//            'is_active',
            [
                'label' => 'IsActive?',
                'attribute' => 'is_active',
                'vAlign' => 'middle',
                'width' => '50px',
                'value' => function($model) {
                    return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                },
                'format' => 'raw',
//                'filterType' => GridView::FILTER_SELECT2,
//                'filter' => ['1' => 'Active', '0' => 'Inactive'],
//                'filterWidgetOptions' => [
//                    'pluginOptions' => ['allowClear' => true],
//                ],
//                'filterInputOptions' => ['placeholder' => 'Search...'],
//                'format' => 'raw'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['school-board/view', 'id' => $model->school_board_id, 'edit' => 't']), [
                                    'title' => Yii::t('yii', 'Edit'),
                        ]);
                    }
                        ],
                    ],
                ],
                'responsive' => true,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => false,
                'panel' => [
                    'heading' => ' ',
                    'type' => 'default',
                    //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                    'showFooter' => false
                ],
            ]);
            Pjax::end();
            ?>

</div>
