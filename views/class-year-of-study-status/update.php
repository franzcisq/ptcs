<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYearOfStudyStatus $model
 */

$this->title = 'Update Class Year Of Study Status: ' . ' ' . $model->class_year_of_study_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Class Year Of Study Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->class_year_of_study_status_id, 'url' => ['view', 'id' => $model->class_year_of_study_status_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="class-year-of-study-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
