<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYearOfStudyStatus $model
 */

$this->title = 'Create Class Year Of Study Status';
$this->params['breadcrumbs'][] = ['label' => 'Class Year Of Study Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="class-year-of-study-status-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
