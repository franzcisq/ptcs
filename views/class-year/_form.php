<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYear $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="class-year-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'academic_year_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Academic Year ID...']], 

'year_number'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Year Number...']], 

'grading_system_version_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Grading System Version ID...']], 

'study_year'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Study Year...']], 

'class_year_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Class Year Status ID...']], 

'approval_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Approval Status ID...']], 

'publish_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Publish Status ID...']], 

'school_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School ID...']], 

'required_year_units'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Required Year Units...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
