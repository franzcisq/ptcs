<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearOfStudyStatus $model
 */

$this->title = 'Update Form Year Of Study Status: ' . ' ' . $model->form_year_of_study_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Form Year Of Study Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->form_year_of_study_status_id, 'url' => ['view', 'id' => $model->form_year_of_study_status_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="form-year-of-study-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
