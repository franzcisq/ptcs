<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\BoardMembers $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="board-members-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [
        
              'user_id' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Staff',
                'options' => [

                    'data' => yii\helpers\ArrayHelper::map(\app\models\Users::find()->all(), 'user_id', 'firstname'),
                    'options' => [
                        'prompt' => '',
                    //'disabled' => $model->isNewrecord ? false : true,
                    ],
                ],
               
            ],

//'school_board_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Board ID...']], 

//'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...']], 

//'is_active'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Active...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
