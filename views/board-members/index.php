<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\BoardMembersSearch $searchModel
 */

$this->title = 'Board Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-members-index">
    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'board_members_id',
            //'school_board_id',
//            'user_id',
//            'is_active',
              [
                      'label'=>'Member',
                      'attribute'=>'user_id',
                      'value'=>  function ($model){
                          return ucwords(\app\models\Users::findOne($model->user_id)->firstname).'  '.  ucwords(\app\models\Users::findOne($model->user_id)->middlename).' '.  ucwords(\app\models\Users::findOne($model->user_id)->surname).' '.'['.\app\models\StaffPositions::findOne(\app\models\Users::findOne($model->user_id)->staff_position_id)->position_name.']';
                      }
                      
                  ],
                                                              [
            'label' => 'Is Active?',
            'attribute' => 'is_active',
            'vAlign' => 'middle',
            'width' => '10%',
            'value' => function($model) {
                return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
            },
            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
        ],

//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['board-members/view','id' => $model->board_members_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,

        'panel' => [
            'heading'=>' '.  \app\models\SchoolBoard::findOne($school_board_id)->name,
            'type'=>'default',
            //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>
    
    
      <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


</div>


