<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\BoardMembers $model
 */

$this->title = 'Update Board Members: ' . ' ' . $model->board_members_id;
$this->params['breadcrumbs'][] = ['label' => 'Board Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->board_members_id, 'url' => ['view', 'id' => $model->board_members_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="board-members-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
