<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormDescription $model
 */

$this->title = 'Add Form ';
$this->params['breadcrumbs'][] = ['label' => 'Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-description-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
