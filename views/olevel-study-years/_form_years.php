<?php

use kartik\tabs\TabsX;

$class_years = app\models\FormYear::find()->where("fy_id = :fy_id  AND school_id = :school_id ", [':fy_id' => $model_fy->fy_id, ':school_id'=>$school_id])->orderBy("fy_id ASC")->all();
$items = [];
$arrayItem = [
    'label' => 'Year Details',
    'content' => $this->render('form_year_details', ['model' => $model_fy]),
    'id' => 'form_year_details_tab'
];

array_push($items, $arrayItem);

foreach ($class_years as $key => $value) {
    $arrayItem = [
        'label' => \app\models\FormDescription::find()->where("form_number = {$value->year_number}")->one()->description,
        'content' => $this->render('_form_year_courses', [ 
            'model_fy' => $model_fy,
            'model_fyosc' =>$model_fyosc, 
            'school_id'=>$school_id,
            'SemActiveTab' => $SemActiveTab,
             'academic_year_id' => $academic_year_id,
              'model_acy' => $model_acy,
              'activeTab' => $activeTab,
                'fy_id'=>$value->fy_id,
                 'study_year'=>$value->study_year,
                ]),
        'id' => 'sem_tab_' . $value->fy_id,
        'active' => ('sem_tab_' . $value->fy_id == $SemActiveTab)
    ];
    array_push($items, $arrayItem);
}

echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'bordered' => true,
    'encodeLabels' =>false
]);
?>
    