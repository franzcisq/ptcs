<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
?>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>'&nbsp;',
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            //'py_id',
            //'ps_id',
            //'academic_year_id',
            'study_year',
            //'grading_system_version_id',
            [
                'label'=> 'Grading Score System',
              'attribute'=>'grading_system_version_id',
              'value' => \app\models\GradingSystemVersions::findOne($model->grading_system_version_id)->description,
            ],
//            
//            'gpa_to_sit_supp',
//            'max_failed_courses',
//            'min_gpa_to_carry',
            'required_year_units',
            //'year_contributes_to_gpa',
//            [
//                'attribute' => 'year_contributes_to_gpa',
//                'value'=>$model->year_contributes_to_gpa?'Yes':'No',
//            ]
            
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->fy_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>false,
    ]) ?>



