<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\jui\Dialog;

$this->title = 'Configure Secondary Forms -  ' . ' ' . 'Academic Year:' . ' ' . $model_acy->academic_year . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp';
$this->params['breadcrumbs'][] = ['label' => 'Secondary Schools', 'url' => ['olevel-study-years/index']];
$this->params['breadcrumbs'][] = 'Configure Secondary Forms - ' . $model_acy->academic_year;
?>

<h4><span class="label label-success">School Name </span>:<span class="label label-info"><?= app\models\Schools::findOne($school_id)->school_name. ' '. \app\models\SchoolType::findOne(app\models\Schools::findOne($school_id)->school_type_id)->school_type?></span> &nbsp;&nbsp; 
</h4><br>

<?php
$form_years = app\models\FormYear::find()->where("academic_year_id = :academic_year_id AND school_id = :school_id ", [':academic_year_id' => $model_acy->academic_year_id,':school_id'=>$school_id])->orderBy("study_year ASC")->all();
$items = [];


foreach ($form_years as $key => $value) {

    $arrayItem = [
        'label' => \app\models\FormDescription::find()->where("form_number = {$value->study_year}")->one()->description,
        'content' => $this->render('_form_years', [
            'model_fyosc' => $model_fyosc,
            'school_id'=>$school_id,
            'model_fy' => $value,
            'SemActiveTab' => $SemActiveTab,
             'academic_year_id' => $academic_year_id,
                'model_acy' => $model_acy,
                    'activeTab' => $activeTab,
                ]),
        'id' => 'fy_' . $value->fy_id,
        'active' => ($activeTab == 'fy_' . $value->fy_id)
    ];
    array_push($items, $arrayItem);
}

$arrayItem = [
    'label' => '<i class="glyphicon glyphicon-plus"></i> Add Year',
    'content' => $this->render('_form_year_form', ['model' => $model_fy, 'school_id'=>$school_id]),
    'id' => 'fy_add_year_tab',
    'active' => ($activeTab == 'fy_add_year_tab')
];
array_push($items, $arrayItem);

echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'bordered' => true,
    'encodeLabels' => false
]);

\yii\jui\Dialog::begin([
    'clientOptions' => [
        'title' => 'Adding new Class Year',
        'autoOpen' => false,
        'width' => '900',
        'height' => '500',
    ],
    'options' => [
        'id' => 'new_programme_year_dialog_id',
    ],
]);
?>

<!--<iframe src="<?php echo Url::to(['programme-year/create', 'academic_year_id' => $model_acy->academic_year_id]); ?>" width="100%" height="100%" class="myiframe"> </iframe>-->

<?php
\yii\jui\Dialog::end();
//echo Html::a('<i class="glyphicon glyphicon-plus"></i> Add New Programme Year', "#", ['class' => 'btn btn-success', 'onclick' => "$('#new_programme_year_dialog_id').dialog('open'); return false; ", 'style'=>'margin-top: 10px']);
//echo $this->render('_form_add_study_year', ['model' => $model_fy]);
?>
