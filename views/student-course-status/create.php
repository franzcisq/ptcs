<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentCourseStatus $model
 */

$this->title = 'Create Student Course Status';
$this->params['breadcrumbs'][] = ['label' => 'Student Course Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-course-status-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
