<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolPrivacyStatementSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="school-privacy-statement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'school_privacy_statement _id') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'statement') ?>

    <?= $form->field($model, 'date_created') ?>

    <?= $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
