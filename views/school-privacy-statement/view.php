<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolPrivacyStatement $model
 */

$this->title = \app\models\Schools::findOne($model->school_id)->school_name;
$this->params['breadcrumbs'][] = ['label' => 'School Privacy Statements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-privacy-statement-view">
    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>'&nbsp',
            'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            //'school_privacy_statement _id',
//            'school_id',
            [
                'label'=>'School',
                'attribute'=>'school_id',
                'value'=>  \app\models\Schools::findOne($model->school_id)->school_name,
                
            ],
            'statement:ntext',
//            [
//                'attribute'=>'date_created',
//                'format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A'],
//                'type'=>DetailView::INPUT_WIDGET,
//                'widgetOptions'=> [
//                    'class'=>DateControl::classname(),
//                    'type'=>DateControl::FORMAT_DATETIME
//                ]
//            ],
            'created_by',
            'is_active',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->school_privacy_statement_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
