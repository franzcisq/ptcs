<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Logins $model
 */

$this->title = 'Create Logins';
$this->params['breadcrumbs'][] = ['label' => 'Logins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logins-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
