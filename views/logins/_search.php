<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LoginsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="logins-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'login_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'ip_address') ?>

    <?= $form->field($model, 'details') ?>

    <?= $form->field($model, 'datecreated') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
