<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolDonors $model
 */

$this->title = 'Update School Donors: ' . ' ' . $model->school_donor_id;
$this->params['breadcrumbs'][] = ['label' => 'School Donors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->school_donor_id, 'url' => ['view', 'id' => $model->school_donor_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="school-donors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
