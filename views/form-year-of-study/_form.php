<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearOfStudy $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="form-year-of-study-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'fy_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fy ID...']], 

'fyos_description'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fyos Description...', 'maxlength'=>100]], 

'fyos_number'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fyos Number...']], 

'fyos_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fyos Status ID...']], 

'approval_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Approval Status ID...']], 

'publish_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Publish Status ID...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
