<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */
$user = Yii::$app->user->identity->firstname . " " . Yii::$app->user->identity->surname . " (" . Yii::$app->user->identity->username . ")";
$this->title = 'Update: ' . ' ' . $user;
$this->params['breadcrumbs'][] = ['label' => 'User Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user, 'url' => ['view', 'id'=>$model->user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-update">

    <?=
    $this->render('_updateidentification_form', [
        'model' => $model,
    ])
    ?>

</div>
