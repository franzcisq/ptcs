<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */

$this->title = 'Add User';
$this->params['breadcrumbs'][] = ['label' => 'User Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
