<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */
$user = Yii::$app->user->identity->firstname . " " . Yii::$app->user->identity->surname . " (" . Yii::$app->user->identity->username . ")";
$this->title =  ' ' . $user.','.' '.'Add|Change Profile Photo';
$this->params['breadcrumbs'][] = ['label' => 'My Profile', 'url' => ['profile']];
$this->params['breadcrumbs'][] = 'Update Profile Picture';
?>
<div class="users-update">

    <?=
    $this->render('_updatepic_form', [
        'model' => $model,
    ])
    ?>

</div>
