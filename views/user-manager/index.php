<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\editable\Editable;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\UsersSearch $searchModel
 */
$this->title = 'User Manager';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">


    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'user_id',
//            'student_status_id',
//            'registration_number',
            [
                'attribute' => 'username',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => $model->username,
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(app\models\Users::find()->orderBy('user_id')->where("user_type = 2")->asArray()->all(), 'username', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            //'firstname',
            [
                'attribute' => 'firstname',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => $model->firstname,
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(app\models\Users::find()->orderBy('user_id')->where("user_type = 2")->asArray()->all(), 'firstname', 'firstname'),
                //'filter'=>ArrayHelper::map(app\models\Users::findBySql("select users.*, concat(ucase(`users`.`surname`),', ',`users`.`firstname`,' ',ifnull(`users`.`middlename`,'')) AS `firstname` from  users")->orderBy('user_id')->asArray()->all(), 'firstname', 'firstname'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            //'surname', 
            [
                'attribute' =>'surname',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => $model->surname,
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(app\models\Users::find()->orderBy('user_id')->where("user_type = 2")->asArray()->all(), 'surname', 'surname'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],

   
//            'middlename',
//            'ps_id',
//            'sex',
//            'date_of_birth',
//            'place_of_birth',
//            'mailing_address',
//            'telephone_no',
            'email_address:email',
//            'admission_date',
//            'sponsorship_type',
//            'status_comments',
//            ['attribute'=>'completion_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
            //'username',
//            'password',
            //'is_active', 
              [
                'label' => 'User Type',
                'attribute' =>'user_type',
                'vAlign' => 'middle',
                'width' => '100px',
                'value' => function($model) {
                    return \app\models\Users::getUserType($model->user_type);
                },
                'format' => 'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['2' => 'Garage Owner', '3' => 'Customer'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'label' => 'IsActive?',
                'attribute' =>'is_active',
                'vAlign' => 'middle',
                'width' => '30px',
                'value' => function($model) {
                    return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                },
                'format' => 'raw',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['1' => 'Active', '0' => 'Inactive'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
//            'login_counts',
//            ['attribute'=>'last_login','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            'user_type',
//            'staff_position_id',
//            'salutation',
//            ['attribute'=>'last_password_update_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
//            'auth_key',
//            'password_reset_token',
//            ['attribute'=>'created_at','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
//            'created_by',

                  [
                'label' => '',
                'value' => function($model) {
                return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-eye-open"></i>View</span>', Yii::$app->urlManager->createUrl(['user-manager/view', 'id' => $model->user_id, 'view' => 't']), [
                                   'title' => Yii::t('yii', 'View'),
                      ]);
                },
                        'format' => 'raw',
                    ],
                            [
                'label' => '',
                'value' => function($model) {
                
                        $url = Yii::$app->urlManager->createUrl(['user-manager/index', 'id_to_deactivate' => $model->user_id, 'subaction' => 'deactivate']);
                        if ($model->is_active == 1 && $model->user_type != 1) {
                            return Html::a('<span class="label label-danger ">Deactivate</span>', $url, ['title' => Yii::t('yii', 'Deactivate')]);
                        }  else {
                            return ' ';
                        }
                        if ($model->is_active == 0) {
                        $url = Yii::$app->urlManager->createUrl(['user-manager/index', 'id_to_activate' => $model->user_id, 'subaction' => 'activate']);
                        //if ($model->is_active == 0) {
                            return Html::a('<span class="label label-success">Activate</span>', $url, ['title' => Yii::t('yii', 'Activate')]);
                        //} 
                        }
                },
                        'format' => 'raw',
                    ],
//           
                ],
                'responsive' => true,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => false,
                'panel' => [
                    'heading' => ' ',
                    'type' => 'default',
                    'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Add User', ['create'], ['class' => 'btn btn-primary']), 'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                    'showFooter' => false
                ],
            ]);
            Pjax::end();
            
           
            ?>

</div>