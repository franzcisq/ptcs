<script>
   function getUserRoutes(){
       var url = <?php echo '"'.\yii\helpers\Url::to(['/auth-assigned-route/index','user_id'=>$model->user_id]).'"'; ?>;
       $('#iframe_user_routes_id').attr('src', url);
   }
</script>

<?php
use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use kartik\tabs\TabsX;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */
$this->title = $model->firstname . ' ' . $model->middlename . ' ' . $model->surname;
$this->params['breadcrumbs'][] = ['label' => 'User Manager', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>



<div class="users-view">
    <?php
      ob_start();
        
    if ($model->profile_picture == NULL) {
    $picture = 'default-no-image/default-no-profile-image.png';
} else {
    $picture = $model->profile_picture;
}
?>
    <?php

   echo   DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => 'view',
        'bordered' =>$bordered,
        'striped' => $striped,
        'condensed' => $condensed,
        'responsive' => $responsive,
         'hideIfEmpty'=>true,
        'hover' => true,
        'hAlign' => $hAlign,
        'vAlign' => $vAlign,
        'fadeDelay' => $fadeDelay,
        //'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' =>'&nbsp',
            'type' => DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
               [
                'group' => true,
                  'label' => 'Staff Profile'.' '."<i class='glyphicon glyphicon-user'></i>". Html::a("<i>Update</i>&nbsp<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['user-manager/updateidentification', 'id' => $model->user_id]), ['class' => 'btn btn success pull-right']) . '<span></li>',
                'rowOptions' => ['class' => 'default']
            ],
                [
                'group' => true,
                'label' => '   <div class="row">
            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img  src="' .$picture. '" width=800 height=900" alt="Profile Photo">
                </a>
            </div>

        </div>',
            ],
//            'user_id',
//            'student_status_id',
//            'registration_number',
            'surname',
            'firstname',
            'middlename',
//            'ps_id',
//            'sex',
            [
                'attribute'=>'sex',
                'value'=>$model->sex == 'M'? 'Male':'Female',
                
            ],
            'date_of_birth',
            'place_of_birth',
            'mailing_address',
            'telephone_no',
            'email_address:email',
//            'admission_date',
//            'sponsorship_type',
//            'status_comments',
       
            'username',
//            'password',
            //'is_active',
            [
                'attribute'=>'is_active',
                'label'=>'Is Active?',
                'value'=>$model->is_active = 1? Active:Inactive,
                
            ],
            
            'login_counts',
            [
                'attribute' => 'last_login',
                'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A'],
                'type' => DetailView::INPUT_WIDGET,
                'widgetOptions' => [
                    'class' => DateControl::classname(),
                    'type' => DateControl::FORMAT_DATETIME
                ]
            ],
//            'user_type',
            [
                'attribute'=>'user_type',
                'label'=>'User Type',
                'value'=>$model->user_type == '2'? 'Garage Owner':'Normal Customer',
                
            ],
         
            'salutation',
      
              [
                'group' => true,
                'label' => 'Reset Password'.' '."<i class='glyphicon glyphicon-lock'></i>". Html::a("<i>Reset</i>&nbsp<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['user-manager/passwordreset', 'id' => $model->user_id]), ['class' => 'btn btn success pull-right']) . '<span></li>',
                'rowOptions' => ['class' => 'default']
            ],

        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->user_id],
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ],
        'enableEditMode' => false,
    ]);
   
 $user_details_tab = ob_get_contents(); ob_end_clean();
    ?>
    <?php
     $user_roles_tab = "<iframe src='".\yii\helpers\Url::to(['auth-assignment/index', 'user_id' => $model->user_id])."' width='100%' height='350px'></iframe>";
    ?>
    
    <?php
      $items = [
        [
           'label' => 'User Details',
           'content' => $user_details_tab,
           'id' => 'user_details_tab_id',
        ],
            [
            'label' => 'User Roles',
            'content' => $user_roles_tab,
            'id' => 'user_roles_tab_id',
        ],
        [
          'label' => '<div onclick="getUserRoutes()">Actions Depending on Level</div>',
          'content' => "<iframe src='' width='100%' height='350px' id='iframe_user_routes_id'></iframe>",
          'id' => 'actions_tab_id',
           
        ],
        
        [
            'label' => 'Login History',
            'content' => '  <iframe src='.yii\helpers\Url::to(['/user-manager/logins-history/','id' => $model->user_id]).' width="100%" height="350px" > </iframe>',
            'id' => 'login_history_tab_id',
        ],
        [
            'label' => 'Actions',
            'content' => '<iframe src='.yii\helpers\Url::to(['/user-manager/user-actions/','id' => $model->user_id]).' width="100%" height="350px" ></iframe>',
            'id' => 'action_tab_id',
        ],
               
    ];
    echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'bordered' => true,
    'encodeLabels' => false
   ]);
    ?>
</div>
