<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolType $model
 */

$this->title = $model->school_type;
$this->params['breadcrumbs'][] = ['label' => 'School Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-type-view">
    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>' ',
            'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            //'school_type_id',
            'school_type',
            'is_active',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->school_type_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
