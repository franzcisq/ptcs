<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolType $model
 */

$this->title = 'Add School Type';
$this->params['breadcrumbs'][] = ['label' => 'School Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
