<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\AcademicYears $model
 */

$this->title = 'Update Academic Years: ' . ' ' . $model->academic_year;
$this->params['breadcrumbs'][] = ['label' => 'Academic Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->academic_year, 'url' => ['view', 'id' => $model->academic_year_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="academic-years-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
