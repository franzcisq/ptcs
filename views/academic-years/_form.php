<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\AcademicYears $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="academic-years-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 3,
    'attributes' => [

'academic_year'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Academic Year...', 'maxlength'=>250]], 

'order'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Order...']], 

'is_current'=>['type'=> Form::INPUT_CHECKBOX, 'options'=>['placeholder'=>'Enter Is Current...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
