<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionType $model
 */

$this->title = $model->institution_type_name;
$this->params['breadcrumbs'][] = ['label' => 'Institution Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-type-view">
    <div class="page-header">
    </div>



    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>'&nbsp;&nbsp;',

            'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            //'institution_type_id',
            'institution_type_name',
            'order',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->institution_type_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
