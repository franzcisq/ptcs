<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionType $model
 */

$this->title = 'Update Institution Type: ' . ' ' . 'Institution Type'.'#'.$model->institution_type_id;
$this->params['breadcrumbs'][] = ['label' => 'Institution Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' =>'Institution Type'.'#'.$model->institution_type_id, 'url' => ['view', 'id' => $model->institution_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="institution-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
