<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\InstitutionTypeSearch $searchModel
 */

$this->title = 'Institution Type';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-type-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'institution_type_id',
            //'institution_type_name',
                                                      [
    'attribute'=>'institution_type_name', 
    'vAlign'=>'middle',
    'width'=>'620px',
    'value'=>$model->institution_type_name,
    'filterType'=>GridView::FILTER_SELECT2,
    'filter'=>ArrayHelper::map(app\models\InstitutionType::find()->orderBy('institution_type_id')->asArray()->all(), 'institution_type_name', 'institution_type_name'), 
    'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
    'filterInputOptions'=>['placeholder'=>'Search...'],
    'format'=>'raw'
],
           // 'order',
                                                               [
    'attribute'=>'order', 
    'vAlign'=>'middle',
    'width'=>'220px',
    'value'=>$model->order,
    'filterType'=>GridView::FILTER_SELECT2,
    'filter'=>ArrayHelper::map(app\models\InstitutionType::find()->orderBy('institution_type_id')->asArray()->all(), 'order', 'order'), 
    'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
    'filterInputOptions'=>['placeholder'=>'Search...'],
    'format'=>'raw'
],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {update}',
                'buttons' => [
                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['institution-type/view','id' => $model->institution_type_id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> &nbsp; &nbsp; </h3>',

            'type'=>'default',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); ?>

</div>
