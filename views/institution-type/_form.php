<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionType $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="institution-type-form">


    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
            'institution_type_name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Institution Type Name...', 'maxlength' => 150]],
            'order' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>[1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6',7=>'7',8=>'8',9=>'9'],  'options' => ['prompt' => '']],
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>
</div>
