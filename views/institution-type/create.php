<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionType $model
 */

$this->title = 'Create Institution Type';
$this->params['breadcrumbs'][] = ['label' => 'Institution Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-type-create">

    <div class="page-header">
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
