<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystemVersions $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="grading-system-versions-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 2,
    'attributes' => [
        
     'school_type_id' => [
                 'type' => Form::INPUT_WIDGET,
                  'widgetClass' => kartik\select2\Select2::className(),
                  'options'=>[
                      'model'=>$model,
                      'attribute'=>'school_type_id',
                      'data'=>\yii\helpers\ArrayHelper::map(app\models\SchoolType::find()->all(),'school_type_id','school_type'),
                  ]
                 ],
'description'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Description...', 'maxlength'=>255]], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
