<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
?>
<div class="grading-system-index">

    <?php
    $gridColumns = [
        [

            'attribute' => 'grade',
            'label' => 'Grade',
            'value' => $model->grade,
          
        ],
        [

            'attribute' => 'min_score',
            'label' => 'Minimum Score',
            'value' => $model->min_score,
             'format' => 'raw',
            'contentOptions'=>['style'=>'text-align:right'],
        ],
        [

            'attribute' => 'max_score',
            'label' => 'Maximum Score',
            'value' => $model->max_score,
             'format' => 'raw',
            'contentOptions'=>['style'=>'text-align:right'],
        ],
        [

            'attribute' => 'grade_points_formula',
            'label' => 'Grade Points',
            'value' => $model->grade_points_formula,
            'format' => 'raw',
            'contentOptions'=>['style'=>'text-align:right'],
        ],
          [

            'attribute' => 'definition',
            'label' => 'Remarks',
            'value' => $model->definition,
          
        ],
//        [
//            'class' => 'yii\grid\ActionColumn',
//            'template' => '{delete}',
//            'buttons' => [
//                'update' => function ($url, $model) {
//                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['grading-system/view', 'id' => $model->grading_system_id, 'edit' => 't']), [
//                                'title' => Yii::t('yii', 'Edit'),
//                    ]);
//                }
//                    ],
//                    'options' => [
//                        'style' => 'width:20px;'
//                    ],
//                ],
    ];

    echo \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
        ],
        'id' => 'targtet-types-grid-view',
    ]);
    ?>

</div>
