<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use yii\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystemVersions $model
 */
$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Grading System Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grading-system-versions-view">

    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => $this->title,
            'type' => DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            //'grading_system_version_id',
            'description',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->grading_system_version_id],
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ],
        'enableEditMode' => true,
    ])
    ?>
   
    
   

</div>
