<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentStatus $model
 */

$this->title = 'Update Student Status: ' . ' ' . $model->student_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->student_status_id, 'url' => ['view', 'id' => $model->student_status_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
