<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\MessageFromParents $model
 */

$this->title = 'Create Message From Parents';
$this->params['breadcrumbs'][] = ['label' => 'Message From Parents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-from-parents-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
