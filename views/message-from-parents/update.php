<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\MessageFromParents $model
 */

$this->title = 'Update Message From Parents: ' . ' ' . $model->msg_frm_parent_id;
$this->params['breadcrumbs'][] = ['label' => 'Message From Parents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->msg_frm_parent_id, 'url' => ['view', 'id' => $model->msg_frm_parent_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="message-from-parents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
