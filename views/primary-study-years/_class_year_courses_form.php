<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
?>

<div class="programme-year-semester-courses-form">

    <?php
 $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_VERTICAL,
                'action' => \yii\helpers\Url::to(['primary-study-years/configure-study-year',
                    'academic_year_id' => $model_cy->academic_year_id,
                    'activeTab' => 'cy_' . $model_cy->cy_id,
                    'SemActiveTab' => 'sem_tab_' . $model_cy->cy_id,
                    'cy_id' => $model_cy->cy_id,
                     'model' => $model_cyosc,
                    'model_cy' => $model_cy,
                    'school_id'=>$school_id,
                   'academic_year_id' => $academic_year_id,
                    'model_acy' => $model_acy,
                     'cy_id'=>$cy_id,
                    'study_year'=>$study_year,
         
                ]),
                'id' =>$cy_id,
    ]);

    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'course_offering_id' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => kartik\select2\Select2::className(),
                'options' => [
                    'attribute' => 'course_offering_id',
                    'model' => $model,
                     'data' => \yii\helpers\ArrayHelper::map(\app\models\Courses::getCurrentVersionPrimaryCourses($school_id,$cy_id), 'course_offering_id', 'course_and_title'),
                    'options' => [
                        'id' => "select2_" .$model_cy->cy_id,
                    ]
                ]
            ],
        ]
    ]);

    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' =>3,
        'attributes' => [
           'pass_grade_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \yii\helpers\ArrayHelper::map(\app\models\GradingSystem::find()->where("grading_system_version_id = {$model_cy->grading_system_version_id} ")->all(), 'grading_system_id', 'grade'), 'options' => ['prompt' => '']],
//            'after_supp_grade_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \yii\helpers\ArrayHelper::map(\app\models\GradingSystem::find()->where("grading_system_version_id = {$model_cy->grading_system_version_id} ")->all(), 'grading_system_id', 'grade'), 'options' => ['prompt' => '']],
//            'after_carryover_grade_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \yii\helpers\ArrayHelper::map(\app\models\GradingSystem::find()->where("grading_system_version_id = {$model_cy->grading_system_version_id} ")->all(), 'grading_system_id', 'grade'), 'options' => ['prompt' => '']],
//            'allowed_to_supp_grade_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \yii\helpers\ArrayHelper::map(\app\models\GradingSystem::find()->where("grading_system_version_id = {$model_cy->grading_system_version_id} ")->all(), 'grading_system_id', 'grade'), 'options' => ['prompt' => '']],
//            'allowed_to_repeat_grade_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \yii\helpers\ArrayHelper::map(\app\models\GradingSystem::find()->where("grading_system_version_id = {$model_cy->grading_system_version_id} ")->all(), 'grading_system_id', 'grade'), 'options' => ['prompt' => '']],
//            'units' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Units...']],
            'is_core' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => ['1' => 'Core', '0' => 'Optional'], 'options' => ['prompt' => '']],
//            //'py_semester_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Py Semester ID...']],
//            'CA_Weight' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Ca  Weight...']],
//            'UE_Weight' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Ue  Weight...']],
//            'minimum_CA' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Ue  Weight...']],
//            'minimum_UE' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Ue  Weight...']],
//            'contributes_to_year_gpa' => ['type' => Form::INPUT_CHECKBOX],
            'contribute_to_final_result' => ['type' => Form::INPUT_CHECKBOX],
        ]
    ]);
    echo Html::submitButton('Add', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>