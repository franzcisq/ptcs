<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClassCourses $model
 */

$this->title = 'Create Student Class Courses';
$this->params['breadcrumbs'][] = ['label' => 'Student Class Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-class-courses-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
