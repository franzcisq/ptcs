<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClassCourses $model
 */

$this->title = 'Update Student Class Courses: ' . ' ' . $model->scc_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Class Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->scc_id, 'url' => ['view', 'id' => $model->scc_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-class-courses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
