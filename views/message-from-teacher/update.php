<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\MessageFromTeacher $model
 */

$this->title = 'Update Message From Teacher: ' . ' ' . $model->msg_frm_teacher_id;
$this->params['breadcrumbs'][] = ['label' => 'Message From Teachers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->msg_frm_teacher_id, 'url' => ['view', 'id' => $model->msg_frm_teacher_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="message-from-teacher-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
