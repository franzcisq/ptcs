<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\MessageFromTeacher $model
 */

$this->title = 'Create Message From Teacher';
$this->params['breadcrumbs'][] = ['label' => 'Message From Teachers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-from-teacher-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
