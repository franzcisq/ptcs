<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\HeadmasterStatement $model
 */

$this->title = 'Create Headmaster Statement';
$this->params['breadcrumbs'][] = ['label' => 'Headmaster Statements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="headmaster-statement-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
