<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolGallery $model
 */

$this->title = 'Update School Gallery: ' . ' ' . $model->school_gallery_id;
$this->params['breadcrumbs'][] = ['label' => 'School Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->school_gallery_id, 'url' => ['view', 'id' => $model->school_gallery_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="school-gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
