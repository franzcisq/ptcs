<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYearOfStudyCourses $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="class-year-of-study-courses-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'cy_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Cy ID...']], 

'course_offering_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Course Offering ID...']], 

'pass_grade_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Pass Grade ID...']], 

'contribute_to_final_result'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Contribute To Final Result...']], 

'is_core'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Core...']], 

'school_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School ID...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
