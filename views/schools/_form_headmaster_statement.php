<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\HeadmasterStatement $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="headmaster-statement-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

//'school_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School ID...']], 
//
//'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...']], 

            'statement' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Headmaster Statement...', 'rows' => 6]],
//'is_active'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Active...']], 
//'date_created'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 
        ]
    ]);
     echo Html::submitButton(Yii::t('app', 'Add') , ['class' => 'btn btn-success']);
    ActiveForm::end();
    ?>

</div>
