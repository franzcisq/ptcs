<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolBoard $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="school-board-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 2,
    'attributes' => [

//'school_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School ID...']], 

'name'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Board Name...', 'maxlength'=>250]], 

'description'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Board Description...','rows'=> 6]], 

//'is_active'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Active...']], 

    ]


    ]);
     echo Html::submitButton(Yii::t('app', 'Add') , ['class' => 'btn btn-success']);
    ActiveForm::end(); ?>

</div>
