<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Sponsors $model
 */

$this->title = $model->sponsor_name;
$this->params['breadcrumbs'][] = ['label' => 'Sponsors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sponsors-view">
    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>' ',
            'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            //'sponsor_id',
            'sponsor_name',
            'sponsor_description:ntext',
            'address',
            'e_mail',
            'phone_no',
            'fax',
            'logo',
            'is_active',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->sponsor_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>false,
    ]) ?>

</div>
