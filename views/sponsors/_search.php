<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\SponsorsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="sponsors-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sponsor_id') ?>

    <?= $form->field($model, 'sponsor_name') ?>

    <?= $form->field($model, 'sponsor_description') ?>

    <?= $form->field($model, 'address') ?>

    <?= $form->field($model, 'e_mail') ?>

    <?php // echo $form->field($model, 'phone_no') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
